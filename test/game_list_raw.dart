var gameListRawString =
'''
{
  "count": 237,
  "next": "https://api.rawg.io/api/games?dates=2020-12-21%2C2021-12-21&key=02ef6ba5d13444ee86bad607e8bce3f4&ordering=-released&page=2&page_size=20&platforms=187",
  "previous": null,
  "results": [
    {
      "slug": "five-nights-at-freddys-security-breach",
      "name": "Five Nights at Freddy's: Security Breach",
      "playtime": 9,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-12-16",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/f5b/f5b0a8232e747c03aa6b56ce2d2af49a.jpg",
      "rating": 2.93,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 13,
          "percent": 30.23
        },
        {
          "id": 1,
          "title": "skip",
          "count": 13,
          "percent": 30.23
        },
        {
          "id": 3,
          "title": "meh",
          "count": 12,
          "percent": 27.91
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 5,
          "percent": 11.63
        }
      ],
      "ratings_count": 43,
      "reviews_text_count": 0,
      "added": 167,
      "added_by_status": {
        "yet": 10,
        "owned": 93,
        "beaten": 30,
        "toplay": 19,
        "dropped": 11,
        "playing": 4
      },
      "metacritic": 64,
      "suggestions_count": 251,
      "updated": "2023-06-06T03:29:57",
      "id": 494383,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205673,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34451,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 32049,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 30012,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30493,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45833,
          "image_background": "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14299,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 42394,
          "name": "Глубокий сюжет",
          "slug": "glubokii-siuzhet",
          "language": "rus",
          "games_count": 8979,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 18160,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 28487,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 42429,
          "name": "От первого лица",
          "slug": "ot-pervogo-litsa",
          "language": "rus",
          "games_count": 7542,
          "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
        },
        {
          "id": 16,
          "name": "Horror",
          "slug": "horror",
          "language": "eng",
          "games_count": 41870,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 6,
          "name": "Exploration",
          "slug": "exploration",
          "language": "eng",
          "games_count": 19339,
          "image_background": "https://media.rawg.io/media/games/e6d/e6de699bd788497f4b52e2f41f9698f2.jpg"
        },
        {
          "id": 15,
          "name": "Stealth",
          "slug": "stealth",
          "language": "eng",
          "games_count": 5708,
          "image_background": "https://media.rawg.io/media/games/c24/c24ec439abf4a2e92f3429dfa83f7f94.jpg"
        },
        {
          "id": 42439,
          "name": "Стелс",
          "slug": "stels",
          "language": "rus",
          "games_count": 1565,
          "image_background": "https://media.rawg.io/media/games/7f6/7f6cd70ba2ad57053b4847c13569f2d8.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13460,
          "image_background": "https://media.rawg.io/media/games/9aa/9aa42d16d425fa6f179fc9dc2f763647.jpg"
        },
        {
          "id": 42562,
          "name": "Для всей семьи",
          "slug": "dlia-vsei-semi",
          "language": "rus",
          "games_count": 5526,
          "image_background": "https://media.rawg.io/media/games/04a/04a7e7e185fb51493bdcbe1693a8b3dc.jpg"
        },
        {
          "id": 37796,
          "name": "exclusive",
          "slug": "exclusive",
          "language": "eng",
          "games_count": 4506,
          "image_background": "https://media.rawg.io/media/games/cd3/cd3c9c7d3e95cb1608fd6250f1b90b7a.jpg"
        },
        {
          "id": 42471,
          "name": "Хоррор на выживание",
          "slug": "khorror-na-vyzhivanie",
          "language": "rus",
          "games_count": 2205,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 17,
          "name": "Survival Horror",
          "slug": "survival-horror",
          "language": "eng",
          "games_count": 7576,
          "image_background": "https://media.rawg.io/media/games/4fb/4fb548e4816c84d1d70f1a228fb167cc.jpg"
        },
        {
          "id": 107,
          "name": "Family Friendly",
          "slug": "family-friendly",
          "language": "eng",
          "games_count": 5235,
          "image_background": "https://media.rawg.io/media/games/78d/78dfae12fb8c5b16cd78648553071e0a.jpg"
        },
        {
          "id": 42601,
          "name": "Цветастая",
          "slug": "tsvetastaia",
          "language": "rus",
          "games_count": 9202,
          "image_background": "https://media.rawg.io/media/games/0be/0bea0a08a4d954337305391b778a7f37.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5844,
          "image_background": "https://media.rawg.io/media/games/d1c/d1cd8a226cb224357c1f59605577cbf2.jpg"
        },
        {
          "id": 165,
          "name": "Colorful",
          "slug": "colorful",
          "language": "eng",
          "games_count": 17780,
          "image_background": "https://media.rawg.io/media/games/8e4/8e4de3f54ac659e08a7ba6a2b731682a.jpg"
        },
        {
          "id": 42405,
          "name": "Сексуальный контент",
          "slug": "seksualnyi-kontent",
          "language": "rus",
          "games_count": 4547,
          "image_background": "https://media.rawg.io/media/games/eb5/eb514db62d397c64288160d5bd8fd67a.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 77232,
          "image_background": "https://media.rawg.io/media/games/74c/74c68a8de3d4983ff932dd456ac2dc66.jpg"
        },
        {
          "id": 50,
          "name": "Sexual Content",
          "slug": "sexual-content",
          "language": "eng",
          "games_count": 4568,
          "image_background": "https://media.rawg.io/media/games/df9/df988191048e92cf86dabd2987c47b62.jpg"
        },
        {
          "id": 218,
          "name": "Multiple Endings",
          "slug": "multiple-endings",
          "language": "eng",
          "games_count": 7070,
          "image_background": "https://media.rawg.io/media/games/1aa/1aaf454e0d3809ba1c34df4514492237.jpg"
        },
        {
          "id": 197,
          "name": "Robots",
          "slug": "robots",
          "language": "eng",
          "games_count": 7735,
          "image_background": "https://media.rawg.io/media/games/cd3/cd3c9c7d3e95cb1608fd6250f1b90b7a.jpg"
        },
        {
          "id": 42630,
          "name": "Роботы",
          "slug": "roboty",
          "language": "rus",
          "games_count": 1333,
          "image_background": "https://media.rawg.io/media/screenshots/dc2/dc2814dc50d61be1ea4fcd5d3c03ddb6.jpg"
        },
        {
          "id": 42571,
          "name": "Мультипликация",
          "slug": "multiplikatsiia",
          "language": "rus",
          "games_count": 3749,
          "image_background": "https://media.rawg.io/media/screenshots/929/92962e1c3902b9a89b91c09c8f1a17b2.jpg"
        },
        {
          "id": 164,
          "name": "Cartoony",
          "slug": "cartoony",
          "language": "eng",
          "games_count": 3691,
          "image_background": "https://media.rawg.io/media/games/f39/f394c09e4760251c58e9ab9f45d7718c.jpg"
        },
        {
          "id": 66533,
          "name": "Исследования",
          "slug": "issledovaniia",
          "language": "rus",
          "games_count": 4665,
          "image_background": "https://media.rawg.io/media/games/91d/91ddeef8d5ebee7f21faa89efa0f2201.jpg"
        },
        {
          "id": 42513,
          "name": "Триллер",
          "slug": "triller",
          "language": "rus",
          "games_count": 949,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 183,
          "name": "Thriller",
          "slug": "thriller",
          "language": "eng",
          "games_count": 1956,
          "image_background": "https://media.rawg.io/media/games/c2a/c2a7dc4540eb79aaff7099ae691105d3.jpg"
        },
        {
          "id": 64060,
          "name": "Несколько концовок",
          "slug": "neskolko-kontsovok",
          "language": "rus",
          "games_count": 2204,
          "image_background": "https://media.rawg.io/media/games/f1d/f1d25c007b9b45c98b57ff9ebbca9692.jpg"
        },
        {
          "id": 42697,
          "name": "Искусственный интеллект",
          "slug": "iskusstvennyi-intellekt",
          "language": "rus",
          "games_count": 947,
          "image_background": "https://media.rawg.io/media/screenshots/5d7/5d724d1575ce237e8f1cf8ffab0d4470.jpg"
        },
        {
          "id": 202,
          "name": "Artificial Intelligence",
          "slug": "artificial-intelligence",
          "language": "eng",
          "games_count": 922,
          "image_background": "https://media.rawg.io/media/screenshots/54d/54d069170e0e4e6f61ba4a6f4a03e173.jpg"
        }
      ],
      "esrb_rating": {
        "id": 3,
        "name": "Teen",
        "slug": "teen",
        "name_en": "Teen",
        "name_ru": "С 13 лет"
      },
      "user_game": null,
      "reviews_count": 43,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/f5b/f5b0a8232e747c03aa6b56ce2d2af49a.jpg"
        },
        {
          "id": 3157696,
          "image": "https://media.rawg.io/media/screenshots/b0f/b0f861b514a34b00a6053e900a332cce.jpg"
        },
        {
          "id": 3157697,
          "image": "https://media.rawg.io/media/screenshots/7fc/7fc885d61e3c201a494976e3c9c8f5f7.jpg"
        },
        {
          "id": 3157698,
          "image": "https://media.rawg.io/media/screenshots/e62/e6256b1e3d5dfffa027ae946db07d1ff.jpg"
        },
        {
          "id": 3157699,
          "image": "https://media.rawg.io/media/screenshots/bd9/bd9849456ac0a56cf8f1914de031abc4.jpg"
        },
        {
          "id": 3157700,
          "image": "https://media.rawg.io/media/screenshots/4b9/4b9d2db680a57f516d9c8150c5cb338c.jpg"
        },
        {
          "id": 3157701,
          "image": "https://media.rawg.io/media/screenshots/215/215f0806391d2fae9dce2f8b18d4c725.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "alfred-hitchcock-vertigo",
      "name": "Alfred Hitchcock – Vertigo",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-16",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/074/074e642bdfa4a6b774977a2222dfd4ea.jpg",
      "rating": 3.84,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 8,
          "percent": 42.11
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 6,
          "percent": 31.58
        },
        {
          "id": 3,
          "title": "meh",
          "count": 3,
          "percent": 15.79
        },
        {
          "id": 1,
          "title": "skip",
          "count": 2,
          "percent": 10.53
        }
      ],
      "ratings_count": 19,
      "reviews_text_count": 0,
      "added": 189,
      "added_by_status": {
        "yet": 16,
        "owned": 112,
        "beaten": 12,
        "toplay": 40,
        "dropped": 7,
        "playing": 2
      },
      "metacritic": 62,
      "suggestions_count": 172,
      "updated": "2023-05-13T10:21:05",
      "id": 622518,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205645,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34427,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29993,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30483,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14296,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 9396,
          "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
        },
        {
          "id": 42441,
          "name": "От третьего лица",
          "slug": "ot-tretego-litsa",
          "language": "rus",
          "games_count": 4907,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 6,
          "name": "Exploration",
          "slug": "exploration",
          "language": "eng",
          "games_count": 19332,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13455,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 42477,
          "name": "Мрачная",
          "slug": "mrachnaia",
          "language": "rus",
          "games_count": 3574,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 14083,
          "image_background": "https://media.rawg.io/media/games/4e6/4e6e8e7f50c237d76f38f3c885dae3d2.jpg"
        },
        {
          "id": 42390,
          "name": "Решения с последствиями",
          "slug": "resheniia-s-posledstviiami",
          "language": "rus",
          "games_count": 3769,
          "image_background": "https://media.rawg.io/media/games/704/704f831d2d132e9614931f1c4eab9e86.jpg"
        },
        {
          "id": 145,
          "name": "Choices Matter",
          "slug": "choices-matter",
          "language": "eng",
          "games_count": 3559,
          "image_background": "https://media.rawg.io/media/games/d8f/d8f3b28fc747ed6f92943cdd33fb91b5.jpeg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5839,
          "image_background": "https://media.rawg.io/media/games/341/3413d7275fb1e919f00a925df8288b77.jpg"
        },
        {
          "id": 110,
          "name": "Cinematic",
          "slug": "cinematic",
          "language": "eng",
          "games_count": 1394,
          "image_background": "https://media.rawg.io/media/games/7ac/7aca7ccf0e70cd0974cb899ab9e5158e.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 77224,
          "image_background": "https://media.rawg.io/media/screenshots/066/066eb1b7a3f332b8089645fbf8c3ebdc.jpg"
        },
        {
          "id": 42623,
          "name": "Кинематографичная",
          "slug": "kinematografichnaia",
          "language": "rus",
          "games_count": 1301,
          "image_background": "https://media.rawg.io/media/games/708/7080e6c87e0825cb02888bf3c44b3889.jpg"
        },
        {
          "id": 142,
          "name": "Detective",
          "slug": "detective",
          "language": "eng",
          "games_count": 2687,
          "image_background": "https://media.rawg.io/media/games/6cc/6cc68fa183b905ac9d85efb9797776f6.jpg"
        },
        {
          "id": 42526,
          "name": "Детектив",
          "slug": "detektiv",
          "language": "rus",
          "games_count": 1249,
          "image_background": "https://media.rawg.io/media/games/b2c/b2c9c6115114c8f7d461b5430e8a7d4a.jpg"
        },
        {
          "id": 209,
          "name": "Drama",
          "slug": "drama",
          "language": "eng",
          "games_count": 2755,
          "image_background": "https://media.rawg.io/media/games/b6b/b6b20bfc4b34e312dbc8aac53c95a348.jpg"
        },
        {
          "id": 42650,
          "name": "Драма",
          "slug": "drama-2",
          "language": "rus",
          "games_count": 2146,
          "image_background": "https://media.rawg.io/media/games/6c1/6c1eecf30e3c34e79bbf86698b1e6515.jpg"
        },
        {
          "id": 200,
          "name": "Narration",
          "slug": "narration",
          "language": "eng",
          "games_count": 1480,
          "image_background": "https://media.rawg.io/media/games/313/3137f0b7fb36b53e34abcde6e7607909.jpg"
        },
        {
          "id": 222,
          "name": "Choose Your Own Adventure",
          "slug": "choose-your-own-adventure",
          "language": "eng",
          "games_count": 2201,
          "image_background": "https://media.rawg.io/media/games/214/2140885d34e3a3398b45036e5d870971.jpg"
        },
        {
          "id": 66533,
          "name": "Исследования",
          "slug": "issledovaniia",
          "language": "rus",
          "games_count": 4658,
          "image_background": "https://media.rawg.io/media/screenshots/8ec/8ece4995d49eab2c9771546fdd48388d.jpg"
        },
        {
          "id": 42513,
          "name": "Триллер",
          "slug": "triller",
          "language": "rus",
          "games_count": 948,
          "image_background": "https://media.rawg.io/media/games/4a0/4a0c82a355ccf7c9d192de0c4f983af8.jpg"
        },
        {
          "id": 42655,
          "name": "Психологическая",
          "slug": "psikhologicheskaia",
          "language": "rus",
          "games_count": 1008,
          "image_background": "https://media.rawg.io/media/screenshots/7cc/7cce32c1c8f43462413a314c02adbd31_XIUrd4S.jpg"
        },
        {
          "id": 288,
          "name": "Interactive Fiction",
          "slug": "interactive-fiction",
          "language": "eng",
          "games_count": 2068,
          "image_background": "https://media.rawg.io/media/games/2e1/2e187b31e5cee21c110bd16798d75fab.jpg"
        },
        {
          "id": 285,
          "name": "Psychological",
          "slug": "psychological",
          "language": "eng",
          "games_count": 1106,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 183,
          "name": "Thriller",
          "slug": "thriller",
          "language": "eng",
          "games_count": 1955,
          "image_background": "https://media.rawg.io/media/games/ceb/ceb68aef14fbe54f0610acd8de480f64.jpg"
        },
        {
          "id": 326,
          "name": "Investigation",
          "slug": "investigation",
          "language": "eng",
          "games_count": 1709,
          "image_background": "https://media.rawg.io/media/games/ceb/ceb68aef14fbe54f0610acd8de480f64.jpg"
        },
        {
          "id": 42646,
          "name": "Беседы",
          "slug": "besedy",
          "language": "rus",
          "games_count": 1105,
          "image_background": "https://media.rawg.io/media/screenshots/d9e/d9e5b749589cc80daffba66f25d6d4ab.jpg"
        },
        {
          "id": 342,
          "name": "Conversation",
          "slug": "conversation",
          "language": "eng",
          "games_count": 1445,
          "image_background": "https://media.rawg.io/media/games/74a/74a324529a14915adfc887d257f16f86.jpg"
        },
        {
          "id": 59333,
          "name": "Повествовательная",
          "slug": "povestvovatelnaia",
          "language": "rus",
          "games_count": 1032,
          "image_background": "https://media.rawg.io/media/games/525/525ddc0a9f22c944af01f074e8983ffe.jpg"
        },
        {
          "id": 66535,
          "name": "Интерактивная литература",
          "slug": "interaktivnaia-literatura",
          "language": "rus",
          "games_count": 1365,
          "image_background": "https://media.rawg.io/media/games/e33/e333678e6e7357d762875b3dcffdabe0.jpg"
        },
        {
          "id": 66540,
          "name": "Выбери себе приключение",
          "slug": "vyberi-sebe-prikliuchenie",
          "language": "rus",
          "games_count": 1497,
          "image_background": "https://media.rawg.io/media/games/b77/b77a93a91e7c0bcdc87e116d366d12c9.jpg"
        },
        {
          "id": 66534,
          "name": "Расследования",
          "slug": "rassledovaniia",
          "language": "rus",
          "games_count": 809,
          "image_background": "https://media.rawg.io/media/games/882/88216bc5c4942c9251078f5596d9ba53.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 19,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/074/074e642bdfa4a6b774977a2222dfd4ea.jpg"
        },
        {
          "id": 2897217,
          "image": "https://media.rawg.io/media/screenshots/af6/af612c4f569dfc5c59752eed7d0728b1.jpg"
        },
        {
          "id": 2897218,
          "image": "https://media.rawg.io/media/screenshots/122/122ace6eb291cbeb864778d33980aa54.jpg"
        },
        {
          "id": 2897219,
          "image": "https://media.rawg.io/media/screenshots/f66/f66e1bd94d9b622412c43a9653fb7f77.jpg"
        },
        {
          "id": 2897220,
          "image": "https://media.rawg.io/media/screenshots/89a/89ad60c0c437109bf78a0fa34b94c3c6.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        }
      ]
    },
    {
      "slug": "clid-the-snail",
      "name": "Clid the snail",
      "playtime": 7,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-15",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/c38/c38d7f5fbbd9af7f68aa24ab1faafc9b.jpg",
      "rating": 0.0,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 2,
          "percent": 40.0
        },
        {
          "id": 3,
          "title": "meh",
          "count": 2,
          "percent": 40.0
        },
        {
          "id": 1,
          "title": "skip",
          "count": 1,
          "percent": 20.0
        }
      ],
      "ratings_count": 5,
      "reviews_text_count": 0,
      "added": 39,
      "added_by_status": {
        "yet": 4,
        "owned": 20,
        "beaten": 1,
        "toplay": 12,
        "dropped": 2
      },
      "metacritic": 60,
      "suggestions_count": 205,
      "updated": "2022-08-10T14:14:02",
      "id": 611597,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205218,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34037,
          "image_background": "https://media.rawg.io/media/games/b45/b45575f34285f2c4479c9a5f719d972e.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 31811,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29785,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30270,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45607,
          "image_background": "https://media.rawg.io/media/games/0be/0bea0a08a4d954337305391b778a7f37.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14195,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42428,
          "name": "Шутер",
          "slug": "shuter",
          "language": "rus",
          "games_count": 6654,
          "image_background": "https://media.rawg.io/media/games/157/15742f2f67eacff546738e1ab5c19d20.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 9324,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 42441,
          "name": "От третьего лица",
          "slug": "ot-tretego-litsa",
          "language": "rus",
          "games_count": 4832,
          "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
        },
        {
          "id": 42480,
          "name": "Фэнтези",
          "slug": "fentezi",
          "language": "rus",
          "games_count": 8080,
          "image_background": "https://media.rawg.io/media/games/21c/21cc15d233117c6809ec86870559e105.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 24338,
          "image_background": "https://media.rawg.io/media/games/4e6/4e6e8e7f50c237d76f38f3c885dae3d2.jpg"
        },
        {
          "id": 42491,
          "name": "Мясо",
          "slug": "miaso",
          "language": "rus",
          "games_count": 3896,
          "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
        },
        {
          "id": 26,
          "name": "Gore",
          "slug": "gore",
          "language": "eng",
          "games_count": 5031,
          "image_background": "https://media.rawg.io/media/games/569/56978b5a77f13aa2ec5d09ec81d01cad.jpg"
        },
        {
          "id": 42402,
          "name": "Насилие",
          "slug": "nasilie",
          "language": "rus",
          "games_count": 4809,
          "image_background": "https://media.rawg.io/media/games/a0e/a0ef08621301a1eab5e04fa5c96978fa.jpeg"
        },
        {
          "id": 34,
          "name": "Violent",
          "slug": "violent",
          "language": "eng",
          "games_count": 5850,
          "image_background": "https://media.rawg.io/media/games/5fa/5fae5fec3c943179e09da67a4427d68f.jpg"
        },
        {
          "id": 42477,
          "name": "Мрачная",
          "slug": "mrachnaia",
          "language": "rus",
          "games_count": 3530,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 14038,
          "image_background": "https://media.rawg.io/media/games/ebd/ebdbb7eb52bd58b0e7fa4538d9757b60.jpg"
        },
        {
          "id": 42530,
          "name": "Кастомизация персонажа",
          "slug": "kastomizatsiia-personazha",
          "language": "rus",
          "games_count": 2674,
          "image_background": "https://media.rawg.io/media/games/e44/e445335e611b4ccf03af71fffcbd30a4.jpg"
        },
        {
          "id": 121,
          "name": "Character Customization",
          "slug": "character-customization",
          "language": "eng",
          "games_count": 3453,
          "image_background": "https://media.rawg.io/media/games/33b/33b825c76382931df0fd8ecddf5caebe.jpg"
        },
        {
          "id": 1465,
          "name": "combat",
          "slug": "combat",
          "language": "eng",
          "games_count": 9255,
          "image_background": "https://media.rawg.io/media/games/048/048b46cdc66cbc7e235e1f359c2a77ec.jpg"
        },
        {
          "id": 42515,
          "name": "Вид сверху",
          "slug": "vid-sverkhu",
          "language": "rus",
          "games_count": 4245,
          "image_background": "https://media.rawg.io/media/games/e4a/e4ab7f784bdc38c76ce6e4cef9715d18.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 77083,
          "image_background": "https://media.rawg.io/media/games/d51/d51ada3b94bfd617bf91d4344ab81ce9.jpg"
        },
        {
          "id": 61,
          "name": "Top-Down",
          "slug": "top-down",
          "language": "eng",
          "games_count": 22858,
          "image_background": "https://media.rawg.io/media/games/003/0031c0067559d41df19cf98ad87e02aa.jpg"
        },
        {
          "id": 42607,
          "name": "Шутер с видом сверху",
          "slug": "shuter-s-vidom-sverkhu",
          "language": "rus",
          "games_count": 1650,
          "image_background": "https://media.rawg.io/media/games/003/0031c0067559d41df19cf98ad87e02aa.jpg"
        },
        {
          "id": 236,
          "name": "Top-Down Shooter",
          "slug": "top-down-shooter",
          "language": "eng",
          "games_count": 1522,
          "image_background": "https://media.rawg.io/media/screenshots/fdc/fdc21a8972c4b38fb0e517db7f6e9e23.jpg"
        },
        {
          "id": 58127,
          "name": "Бой",
          "slug": "boi-2",
          "language": "rus",
          "games_count": 4190,
          "image_background": "https://media.rawg.io/media/games/2e7/2e732a02c29c84ca177855848932a5aa.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 5,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/c38/c38d7f5fbbd9af7f68aa24ab1faafc9b.jpg"
        },
        {
          "id": 3140629,
          "image": "https://media.rawg.io/media/screenshots/4c7/4c7bee3c34ca96c7390c9a8075dcd397.jpg"
        },
        {
          "id": 3140630,
          "image": "https://media.rawg.io/media/screenshots/461/461543ee4a831d1c81163cff7a426e8f.jpg"
        },
        {
          "id": 3140631,
          "image": "https://media.rawg.io/media/screenshots/c38/c38d7f5fbbd9af7f68aa24ab1faafc9b.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "aeterna-noctis",
      "name": "Aeterna Noctis",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-14",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/d86/d86c523f20560ca06dd2c86f7ac34571.jpg",
      "rating": 3.3,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 3,
          "percent": 30.0
        },
        {
          "id": 3,
          "title": "meh",
          "count": 3,
          "percent": 30.0
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 2,
          "percent": 20.0
        },
        {
          "id": 1,
          "title": "skip",
          "count": 2,
          "percent": 20.0
        }
      ],
      "ratings_count": 10,
      "reviews_text_count": 0,
      "added": 105,
      "added_by_status": {
        "yet": 10,
        "owned": 61,
        "beaten": 1,
        "toplay": 23,
        "dropped": 6,
        "playing": 4
      },
      "metacritic": 69,
      "suggestions_count": 330,
      "updated": "2023-04-01T21:06:44",
      "id": 650664,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205645,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34427,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 32032,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29993,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30483,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45819,
          "image_background": "https://media.rawg.io/media/games/d1f/d1f872a48286b6b751670817d5c1e1be.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14296,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 29412,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 42394,
          "name": "Глубокий сюжет",
          "slug": "glubokii-siuzhet",
          "language": "rus",
          "games_count": 8974,
          "image_background": "https://media.rawg.io/media/games/26d/26d4437715bee60138dab4a7c8c59c92.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 18155,
          "image_background": "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg"
        },
        {
          "id": 45,
          "name": "2D",
          "slug": "2d",
          "language": "eng",
          "games_count": 187825,
          "image_background": "https://media.rawg.io/media/games/f90/f90ee1a4239247a822771c40488e68c5.jpg"
        },
        {
          "id": 42420,
          "name": "Сложная",
          "slug": "slozhnaia",
          "language": "rus",
          "games_count": 4470,
          "image_background": "https://media.rawg.io/media/games/559/559bc0768f656ad0c63c54b80a82d680.jpg"
        },
        {
          "id": 42480,
          "name": "Фэнтези",
          "slug": "fentezi",
          "language": "rus",
          "games_count": 8190,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 24451,
          "image_background": "https://media.rawg.io/media/screenshots/88b/88b5f27f07d6ca2f8a3cd0b36e03a670.jpg"
        },
        {
          "id": 49,
          "name": "Difficult",
          "slug": "difficult",
          "language": "eng",
          "games_count": 12449,
          "image_background": "https://media.rawg.io/media/games/283/283e7e600366b0da7021883d27159b27.jpg"
        },
        {
          "id": 6,
          "name": "Exploration",
          "slug": "exploration",
          "language": "eng",
          "games_count": 19332,
          "image_background": "https://media.rawg.io/media/games/b7d/b7d3f1715fa8381a4e780173a197a615.jpg"
        },
        {
          "id": 42463,
          "name": "Платформер",
          "slug": "platformer-2",
          "language": "rus",
          "games_count": 6479,
          "image_background": "https://media.rawg.io/media/games/9cc/9cc11e2e81403186c7fa9c00c143d6e4.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13455,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 134,
          "name": "Anime",
          "slug": "anime",
          "language": "eng",
          "games_count": 11017,
          "image_background": "https://media.rawg.io/media/screenshots/6d3/6d367773c06886535620f2d7fb1cb866.jpg"
        },
        {
          "id": 42407,
          "name": "Аниме",
          "slug": "anime-2",
          "language": "rus",
          "games_count": 6906,
          "image_background": "https://media.rawg.io/media/games/972/972aea3c9eb253e893947bec2d2cfbb9.jpg"
        },
        {
          "id": 42469,
          "name": "Вид сбоку",
          "slug": "vid-sboku",
          "language": "rus",
          "games_count": 3037,
          "image_background": "https://media.rawg.io/media/games/f8c/f8c6a262ead4c16b47e1219310210eb3.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5839,
          "image_background": "https://media.rawg.io/media/games/341/3413d7275fb1e919f00a925df8288b77.jpg"
        },
        {
          "id": 113,
          "name": "Side Scroller",
          "slug": "side-scroller",
          "language": "eng",
          "games_count": 9421,
          "image_background": "https://media.rawg.io/media/games/a5a/a5abaa1b5cc1567b026fa3aa9fbd828e.jpg"
        },
        {
          "id": 82,
          "name": "Magic",
          "slug": "magic",
          "language": "eng",
          "games_count": 8090,
          "image_background": "https://media.rawg.io/media/games/886/8868f9ea0c91d45316319ebf5dd4fc64.jpg"
        },
        {
          "id": 42478,
          "name": "Магия",
          "slug": "magiia",
          "language": "rus",
          "games_count": 2732,
          "image_background": "https://media.rawg.io/media/games/cd7/cd78e63236e86f97f4b2e45f3843eb3d.jpg"
        },
        {
          "id": 259,
          "name": "Metroidvania",
          "slug": "metroidvania",
          "language": "eng",
          "games_count": 4036,
          "image_background": "https://media.rawg.io/media/games/c50/c5085506fe4b5e20fc7aa5ace842c20b.jpg"
        },
        {
          "id": 42468,
          "name": "Головоломка-платформер",
          "slug": "golovolomka-platformer",
          "language": "rus",
          "games_count": 2777,
          "image_background": "https://media.rawg.io/media/games/ba0/ba006ef12175ad4773e5964c320099c4.jpg"
        },
        {
          "id": 42462,
          "name": "Метроидвания",
          "slug": "metroidvaniia",
          "language": "rus",
          "games_count": 938,
          "image_background": "https://media.rawg.io/media/games/04a/04a7e7e185fb51493bdcbe1693a8b3dc.jpg"
        },
        {
          "id": 42483,
          "name": "Рисованная графика",
          "slug": "risovannaia-grafika",
          "language": "rus",
          "games_count": 2790,
          "image_background": "https://media.rawg.io/media/games/fa3/fa3dd043cba3a9cbfe3085e75d92bf7e.jpg"
        },
        {
          "id": 258,
          "name": "Hand-drawn",
          "slug": "hand-drawn",
          "language": "eng",
          "games_count": 5679,
          "image_background": "https://media.rawg.io/media/games/416/4164ca654a339af5be8e63cc9c480c70.jpg"
        },
        {
          "id": 42592,
          "name": "Похожа на Dark Souls",
          "slug": "pokhozha-na-dark-souls",
          "language": "rus",
          "games_count": 647,
          "image_background": "https://media.rawg.io/media/games/16a/16a81cc458b0acb6ed2bcfd2a10f1527.jpg"
        },
        {
          "id": 58132,
          "name": "Атмосферная",
          "slug": "atmosfernaia",
          "language": "rus",
          "games_count": 5044,
          "image_background": "https://media.rawg.io/media/games/dbb/dbba6100aae179b5f24052c9141d426d.jpg"
        },
        {
          "id": 66533,
          "name": "Исследования",
          "slug": "issledovaniia",
          "language": "rus",
          "games_count": 4658,
          "image_background": "https://media.rawg.io/media/screenshots/8ec/8ece4995d49eab2c9771546fdd48388d.jpg"
        },
        {
          "id": 295,
          "name": "Soundtrack",
          "slug": "soundtrack",
          "language": "eng",
          "games_count": 2774,
          "image_background": "https://media.rawg.io/media/games/ed0/ed05e3f2303b644e3b95b409a7909f5a.jpg"
        },
        {
          "id": 580,
          "name": "Souls-like",
          "slug": "souls-like",
          "language": "eng",
          "games_count": 1054,
          "image_background": "https://media.rawg.io/media/games/673/673f27be4c5dbf37ce3ed0ed5ddf9d8d.jpg"
        },
        {
          "id": 42698,
          "name": "Саундтрек",
          "slug": "saundtrek",
          "language": "rus",
          "games_count": 422,
          "image_background": "https://media.rawg.io/media/games/d70/d70084559f41e7a7d4b1cc441c163763.jpg"
        },
        {
          "id": 49955,
          "name": "Puzzle Platformer",
          "slug": "puzzle-platformer-2",
          "language": "eng",
          "games_count": 2064,
          "image_background": "https://media.rawg.io/media/screenshots/4b5/4b5f453f554f4b0d1a4ce904bfcb63df.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 10,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/d86/d86c523f20560ca06dd2c86f7ac34571.jpg"
        },
        {
          "id": 3092999,
          "image": "https://media.rawg.io/media/screenshots/57c/57c55406ba1543e9393d438b8d74a0bb.jpg"
        },
        {
          "id": 3093000,
          "image": "https://media.rawg.io/media/screenshots/24a/24a5f211950dc5beb265a943e044d752.jpg"
        },
        {
          "id": 3093001,
          "image": "https://media.rawg.io/media/screenshots/421/4215922321b0b0c271b3f046ce706c9a.jpg"
        },
        {
          "id": 3093002,
          "image": "https://media.rawg.io/media/screenshots/592/592bc9a3dabce8a08c4e4b53f2830178.jpg"
        },
        {
          "id": 3093003,
          "image": "https://media.rawg.io/media/screenshots/592/59272b6f6b919b00f2c7b63d55caa0aa.jpg"
        },
        {
          "id": 3093004,
          "image": "https://media.rawg.io/media/screenshots/a0e/a0e088169d716e768416afb1a02c5f18.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 83,
          "name": "Platformer",
          "slug": "platformer"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "prey-for-the-gods",
      "name": "Praey for the Gods",
      "playtime": 2,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-12-14",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/98c/98cc69167011df30d9e7d1bf3f2988ab.jpg",
      "rating": 3.03,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 13,
          "percent": 36.11
        },
        {
          "id": 3,
          "title": "meh",
          "count": 13,
          "percent": 36.11
        },
        {
          "id": 1,
          "title": "skip",
          "count": 8,
          "percent": 22.22
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 2,
          "percent": 5.56
        }
      ],
      "ratings_count": 36,
      "reviews_text_count": 0,
      "added": 377,
      "added_by_status": {
        "yet": 36,
        "owned": 226,
        "beaten": 14,
        "toplay": 82,
        "dropped": 17,
        "playing": 2
      },
      "metacritic": 54,
      "suggestions_count": 220,
      "updated": "2023-04-29T22:07:18",
      "id": 18856,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205673,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34451,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 32049,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 30012,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45833,
          "image_background": "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14299,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 42400,
          "name": "Атмосфера",
          "slug": "atmosfera",
          "language": "rus",
          "games_count": 6083,
          "image_background": "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg"
        },
        {
          "id": 42401,
          "name": "Отличный саундтрек",
          "slug": "otlichnyi-saundtrek",
          "language": "rus",
          "games_count": 4461,
          "image_background": "https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 17231,
          "image_background": "https://media.rawg.io/media/games/c24/c24ec439abf4a2e92f3429dfa83f7f94.jpg"
        },
        {
          "id": 42412,
          "name": "Ролевая игра",
          "slug": "rolevaia-igra",
          "language": "rus",
          "games_count": 13649,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 42442,
          "name": "Открытый мир",
          "slug": "otkrytyi-mir",
          "language": "rus",
          "games_count": 4432,
          "image_background": "https://media.rawg.io/media/games/d69/d69810315bd7e226ea2d21f9156af629.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 4064,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 9401,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42441,
          "name": "От третьего лица",
          "slug": "ot-tretego-litsa",
          "language": "rus",
          "games_count": 4912,
          "image_background": "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg"
        },
        {
          "id": 42413,
          "name": "Симулятор",
          "slug": "simuliator",
          "language": "rus",
          "games_count": 15049,
          "image_background": "https://media.rawg.io/media/games/174/1743b3dd185bda4a7be349347d4064df.jpg"
        },
        {
          "id": 42480,
          "name": "Фэнтези",
          "slug": "fentezi",
          "language": "rus",
          "games_count": 8195,
          "image_background": "https://media.rawg.io/media/games/63f/63f0e68688cad279ed38cde931dbfcdb.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 24456,
          "image_background": "https://media.rawg.io/media/games/6fc/6fcf4cd3b17c288821388e6085bb0fc9.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 10532,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 1,
          "name": "Survival",
          "slug": "survival",
          "language": "eng",
          "games_count": 7198,
          "image_background": "https://media.rawg.io/media/games/1bd/1bd2657b81eb0c99338120ad444b24ff.jpg"
        },
        {
          "id": 42452,
          "name": "Выживание",
          "slug": "vyzhivanie",
          "language": "rus",
          "games_count": 4689,
          "image_background": "https://media.rawg.io/media/games/daa/daaee07fcb40744d90cf8142f94a241f.jpg"
        },
        {
          "id": 42404,
          "name": "Женщина-протагонист",
          "slug": "zhenshchina-protagonist",
          "language": "rus",
          "games_count": 2413,
          "image_background": "https://media.rawg.io/media/games/3cf/3cff89996570cf29a10eb9cd967dcf73.jpg"
        },
        {
          "id": 42464,
          "name": "Исследование",
          "slug": "issledovanie",
          "language": "rus",
          "games_count": 2979,
          "image_background": "https://media.rawg.io/media/games/d7d/d7d33daa1892e2468cd0263d5dfc957e.jpg"
        },
        {
          "id": 42411,
          "name": "Ранний доступ",
          "slug": "rannii-dostup",
          "language": "rus",
          "games_count": 11799,
          "image_background": "https://media.rawg.io/media/games/66e/66e90c9d7b9a17335b310ceb294e9365.jpg"
        },
        {
          "id": 42506,
          "name": "Тёмное фэнтези",
          "slug": "tiomnoe-fentezi",
          "language": "rus",
          "games_count": 1922,
          "image_background": "https://media.rawg.io/media/games/b4a/b4adf80c36e267b35acc3497ed2af19c.jpg"
        },
        {
          "id": 96,
          "name": "Kickstarter",
          "slug": "kickstarter",
          "language": "eng",
          "games_count": 586,
          "image_background": "https://media.rawg.io/media/games/c32/c323f9c17ac95c363772bb9ff3693dc6.jpg"
        },
        {
          "id": 42580,
          "name": "Создана на пожертвования",
          "slug": "sozdana-na-pozhertvovaniia",
          "language": "rus",
          "games_count": 75,
          "image_background": "https://media.rawg.io/media/games/789/7896837ec22a83e4007018ddd55e8c9a.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 36,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/98c/98cc69167011df30d9e7d1bf3f2988ab.jpg"
        },
        {
          "id": 175442,
          "image": "https://media.rawg.io/media/screenshots/6c0/6c059126592c57188772aa7603e7e164.jpg"
        },
        {
          "id": 175443,
          "image": "https://media.rawg.io/media/screenshots/591/5913a9c8aa71b8c6075e345d526f19a3.jpg"
        },
        {
          "id": 175444,
          "image": "https://media.rawg.io/media/screenshots/626/626d7cffc4b99e0ada1c1428e6def07f.jpg"
        },
        {
          "id": 175445,
          "image": "https://media.rawg.io/media/screenshots/abb/abb98e5ccda847c086ee9063c2e008ae.jpg"
        },
        {
          "id": 175446,
          "image": "https://media.rawg.io/media/screenshots/b09/b0995778094575ec3a67e9d30d761f2c.jpg"
        },
        {
          "id": 175447,
          "image": "https://media.rawg.io/media/screenshots/e68/e68db9985b98d1fe1a118775a71d02af.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        },
        {
          "id": 5,
          "name": "RPG",
          "slug": "role-playing-games-rpg"
        },
        {
          "id": 14,
          "name": "Simulation",
          "slug": "simulation"
        },
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        }
      ]
    },
    {
      "slug": "firegirl-hack-n-splash-rescue",
      "name": "Firegirl: Hack 'n Splash Rescue",
      "playtime": 1,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-13",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/88b/88be0c81d3d58470d45dde9de0898e9f.jpg",
      "rating": 0.0,
      "rating_top": 3,
      "ratings": [
        {
          "id": 3,
          "title": "meh",
          "count": 3,
          "percent": 60.0
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 1,
          "percent": 20.0
        },
        {
          "id": 1,
          "title": "skip",
          "count": 1,
          "percent": 20.0
        }
      ],
      "ratings_count": 5,
      "reviews_text_count": 0,
      "added": 53,
      "added_by_status": {
        "yet": 7,
        "owned": 39,
        "beaten": 1,
        "toplay": 4,
        "dropped": 2
      },
      "metacritic": null,
      "suggestions_count": 326,
      "updated": "2023-04-29T20:53:36",
      "id": 702251,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205468,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34246,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 31930,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29891,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45717,
          "image_background": "https://media.rawg.io/media/games/226/2262cea0b385db6cf399f4be831603b0.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14250,
          "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
        },
        {
          "id": 42394,
          "name": "Глубокий сюжет",
          "slug": "glubokii-siuzhet",
          "language": "rus",
          "games_count": 8926,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 18111,
          "image_background": "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg"
        },
        {
          "id": 45,
          "name": "2D",
          "slug": "2d",
          "language": "eng",
          "games_count": 187760,
          "image_background": "https://media.rawg.io/media/games/85c/85c8ae70e7cdf0105f06ef6bdce63b8b.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 10508,
          "image_background": "https://media.rawg.io/media/games/e3d/e3ddc524c6292a435d01d97cc5f42ea7.jpg"
        },
        {
          "id": 42463,
          "name": "Платформер",
          "slug": "platformer-2",
          "language": "rus",
          "games_count": 6444,
          "image_background": "https://media.rawg.io/media/screenshots/c97/c97b943741f5fbc936fe054d9d58851d.jpg"
        },
        {
          "id": 42415,
          "name": "Пиксельная графика",
          "slug": "pikselnaia-grafika",
          "language": "rus",
          "games_count": 8851,
          "image_background": "https://media.rawg.io/media/games/ffe/ffed87105b14f5beff72ff44a7793fd5.jpg"
        },
        {
          "id": 122,
          "name": "Pixel Graphics",
          "slug": "pixel-graphics",
          "language": "eng",
          "games_count": 90059,
          "image_background": "https://media.rawg.io/media/games/003/0031c0067559d41df19cf98ad87e02aa.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13412,
          "image_background": "https://media.rawg.io/media/games/9aa/9aa42d16d425fa6f179fc9dc2f763647.jpg"
        },
        {
          "id": 42587,
          "name": "Аркада",
          "slug": "arkada",
          "language": "rus",
          "games_count": 7506,
          "image_background": "https://media.rawg.io/media/games/786/7863e587bac630de82fca50d799236a9.jpg"
        },
        {
          "id": 42520,
          "name": "Реиграбельность",
          "slug": "reigrabelnost",
          "language": "rus",
          "games_count": 1835,
          "image_background": "https://media.rawg.io/media/games/f3e/f3eec35c6218dcfd93a537751e6bfa61.jpg"
        },
        {
          "id": 42530,
          "name": "Кастомизация персонажа",
          "slug": "kastomizatsiia-personazha",
          "language": "rus",
          "games_count": 2693,
          "image_background": "https://media.rawg.io/media/games/da1/da1b267764d77221f07a4386b6548e5a.jpg"
        },
        {
          "id": 5,
          "name": "Replay Value",
          "slug": "replay-value",
          "language": "eng",
          "games_count": 1286,
          "image_background": "https://media.rawg.io/media/games/951/951572a3dd1e42544bd39a5d5b42d234.jpg"
        },
        {
          "id": 121,
          "name": "Character Customization",
          "slug": "character-customization",
          "language": "eng",
          "games_count": 3472,
          "image_background": "https://media.rawg.io/media/games/8fc/8fc59e74133fd8a8a436b7e2d0fb36c2.jpg"
        },
        {
          "id": 42601,
          "name": "Цветастая",
          "slug": "tsvetastaia",
          "language": "rus",
          "games_count": 9134,
          "image_background": "https://media.rawg.io/media/games/e74/e74458058b35e01c1ae3feeb39a3f724.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5793,
          "image_background": "https://media.rawg.io/media/screenshots/2fc/2fc6994425146f9dba3133400b414e29.jpg"
        },
        {
          "id": 42419,
          "name": "Рогалик",
          "slug": "rogalik",
          "language": "rus",
          "games_count": 2773,
          "image_background": "https://media.rawg.io/media/games/800/800d07ca648a9778a8230f40088e0866.jpg"
        },
        {
          "id": 165,
          "name": "Colorful",
          "slug": "colorful",
          "language": "eng",
          "games_count": 17713,
          "image_background": "https://media.rawg.io/media/games/bbf/bbf8d74ab64440ad76294cff2f4d9cfa.jpg"
        },
        {
          "id": 639,
          "name": "Roguelike",
          "slug": "roguelike",
          "language": "eng",
          "games_count": 11222,
          "image_background": "https://media.rawg.io/media/screenshots/428/4286a224467623d23706dacb569a2ea1.jpg"
        },
        {
          "id": 42591,
          "name": "Упрощённый рогалик",
          "slug": "uproshchionnyi-rogalik",
          "language": "rus",
          "games_count": 2276,
          "image_background": "https://media.rawg.io/media/games/fad/fadc4be043ed07904012d47cd02671e4.jpg"
        },
        {
          "id": 640,
          "name": "Roguelite",
          "slug": "roguelite",
          "language": "eng",
          "games_count": 5040,
          "image_background": "https://media.rawg.io/media/screenshots/e9c/e9ce72a3e2c1ac344e8876d6bb0dcf50.jpeg"
        },
        {
          "id": 42418,
          "name": "Процедурная генерация",
          "slug": "protsedurnaia-generatsiia",
          "language": "rus",
          "games_count": 2145,
          "image_background": "https://media.rawg.io/media/screenshots/559/559fb67a9c379630bbb821595e102bee_2EQC8gc.jpg"
        },
        {
          "id": 42667,
          "name": "Псевдотрёхмерность",
          "slug": "psevdotriokhmernost",
          "language": "rus",
          "games_count": 1407,
          "image_background": "https://media.rawg.io/media/games/7ba/7baf4663962bad7197e2470d59a6e322.jpg"
        },
        {
          "id": 116,
          "name": "2.5D",
          "slug": "25d",
          "language": "eng",
          "games_count": 1334,
          "image_background": "https://media.rawg.io/media/games/283/283e7e600366b0da7021883d27159b27.jpg"
        },
        {
          "id": 196,
          "name": "Procedural Generation",
          "slug": "procedural-generation",
          "language": "eng",
          "games_count": 6723,
          "image_background": "https://media.rawg.io/media/games/70a/70a7a7b21d8fdf5f19622e5e14599bcd.jpg"
        },
        {
          "id": 59643,
          "name": "Протагонистка",
          "slug": "protagonistka",
          "language": "eng",
          "games_count": 2978,
          "image_background": "https://media.rawg.io/media/games/2d8/2d84abb8b5e5ca1611048f682a61f93b.jpg"
        },
        {
          "id": 49964,
          "name": "Action Roguelike",
          "slug": "action-roguelike-2",
          "language": "eng",
          "games_count": 1432,
          "image_background": "https://media.rawg.io/media/screenshots/6ed/6ed9f124116deafb14c4a53cd078e8e8.jpg"
        },
        {
          "id": 49997,
          "name": "Экшен-рогалик",
          "slug": "ekshen-rogalik",
          "language": "rus",
          "games_count": 1425,
          "image_background": "https://media.rawg.io/media/games/295/295cb3ba936e0c851c326c491b00b978.jpg"
        },
        {
          "id": 50000,
          "name": "Roguevania",
          "slug": "roguevania",
          "language": "eng",
          "games_count": 83,
          "image_background": "https://media.rawg.io/media/screenshots/a8f/a8fb6fa269c5e1faf0329de6f1e4b88f.jpg"
        },
        {
          "id": 59465,
          "name": "Рогалик-метроидвания",
          "slug": "rogalik-metroidvaniia",
          "language": "eng",
          "games_count": 71,
          "image_background": "https://media.rawg.io/media/screenshots/c8b/c8bced38a45f654e4374d28446083a36.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 5,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/88b/88be0c81d3d58470d45dde9de0898e9f.jpg"
        },
        {
          "id": 3132359,
          "image": "https://media.rawg.io/media/screenshots/17a/17a60c565223a7cd015a236a06dd1ab1.jpg"
        },
        {
          "id": 3132360,
          "image": "https://media.rawg.io/media/screenshots/5d8/5d8a59201cc9318e72e3638c6c40ccf0.jpg"
        },
        {
          "id": 3132361,
          "image": "https://media.rawg.io/media/screenshots/f35/f3542ef46d9b8981c365bd95fb11a32e.jpg"
        },
        {
          "id": 3132362,
          "image": "https://media.rawg.io/media/screenshots/87b/87bdab92f5977d3ec3d4c9a6d3627a11.jpg"
        },
        {
          "id": 3132363,
          "image": "https://media.rawg.io/media/screenshots/2d2/2d216f80712b9be34c9c587a9b4aad38.jpg"
        },
        {
          "id": 3132364,
          "image": "https://media.rawg.io/media/screenshots/cc1/cc1029a62d542659018ecafa2245923f.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "wytchwood",
      "name": "Wytchwood",
      "playtime": 1,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        }
      ],
      "released": "2021-12-09",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/058/058544caa3abdd324b09c5a8bec1f899.jpg",
      "rating": 4.0,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 6,
          "percent": 50.0
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 4,
          "percent": 33.33
        },
        {
          "id": 3,
          "title": "meh",
          "count": 1,
          "percent": 8.33
        },
        {
          "id": 1,
          "title": "skip",
          "count": 1,
          "percent": 8.33
        }
      ],
      "ratings_count": 12,
      "reviews_text_count": 0,
      "added": 184,
      "added_by_status": {
        "yet": 10,
        "owned": 133,
        "beaten": 7,
        "toplay": 29,
        "dropped": 3,
        "playing": 2
      },
      "metacritic": 78,
      "suggestions_count": 237,
      "updated": "2023-04-03T01:42:08",
      "id": 49417,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205673,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 30012,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30493,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45833,
          "image_background": "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14299,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 24,
          "name": "RPG",
          "slug": "rpg",
          "language": "eng",
          "games_count": 17231,
          "image_background": "https://media.rawg.io/media/games/c24/c24ec439abf4a2e92f3429dfa83f7f94.jpg"
        },
        {
          "id": 42412,
          "name": "Ролевая игра",
          "slug": "rolevaia-igra",
          "language": "rus",
          "games_count": 13649,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 42399,
          "name": "Казуальная игра",
          "slug": "kazualnaia-igra",
          "language": "rus",
          "games_count": 31538,
          "image_background": "https://media.rawg.io/media/games/852/8522935d8ab27b610a254b52de0da212.jpg"
        },
        {
          "id": 11669,
          "name": "stats",
          "slug": "stats",
          "language": "eng",
          "games_count": 4474,
          "image_background": "https://media.rawg.io/media/games/48e/48e63bbddeddbe9ba81942772b156664.jpg"
        },
        {
          "id": 468,
          "name": "role-playing",
          "slug": "role-playing",
          "language": "eng",
          "games_count": 1486,
          "image_background": "https://media.rawg.io/media/games/fae/faebf3c8cbf30db3f46bfbecf6ada3d6.jpg"
        }
      ],
      "esrb_rating": {
        "id": 2,
        "name": "Everyone 10+",
        "slug": "everyone-10-plus",
        "name_en": "Everyone 10+",
        "name_ru": "С 10 лет"
      },
      "user_game": null,
      "reviews_count": 12,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/058/058544caa3abdd324b09c5a8bec1f899.jpg"
        },
        {
          "id": 698439,
          "image": "https://media.rawg.io/media/screenshots/49d/49def9c9cf7205750f7223948394fdae.jpg"
        },
        {
          "id": 698440,
          "image": "https://media.rawg.io/media/screenshots/b1d/b1dc1159b47113eef49664ff63ea1fe5.jpg"
        },
        {
          "id": 698441,
          "image": "https://media.rawg.io/media/screenshots/df8/df816726ac0895f11a718f354516b037.jpg"
        },
        {
          "id": 698442,
          "image": "https://media.rawg.io/media/screenshots/433/4333e59f058540b903af2c47f09b2dcf.jpg"
        },
        {
          "id": 698443,
          "image": "https://media.rawg.io/media/screenshots/a7c/a7c09ba5a91f038cfaff5d29e2ff306e.jpg"
        },
        {
          "id": 698444,
          "image": "https://media.rawg.io/media/screenshots/058/058544caa3abdd324b09c5a8bec1f899.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 40,
          "name": "Casual",
          "slug": "casual"
        },
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 5,
          "name": "RPG",
          "slug": "role-playing-games-rpg"
        }
      ]
    },
    {
      "slug": "final-fantasy-xiv-endwalker",
      "name": "FINAL FANTASY XIV: Endwalker",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-12-07",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/bf5/bf5ac217a9e10ff98301e8359a870dc1.jpg",
      "rating": 4.55,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 23,
          "percent": 74.19
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 6,
          "percent": 19.35
        },
        {
          "id": 1,
          "title": "skip",
          "count": 2,
          "percent": 6.45
        }
      ],
      "ratings_count": 30,
      "reviews_text_count": 1,
      "added": 86,
      "added_by_status": {
        "yet": 8,
        "owned": 18,
        "beaten": 23,
        "toplay": 20,
        "dropped": 2,
        "playing": 15
      },
      "metacritic": 91,
      "suggestions_count": 449,
      "updated": "2023-05-13T17:31:19",
      "id": 681399,
      "score": null,
      "clip": null,
      "tags": [],
      "esrb_rating": {
        "id": 3,
        "name": "Teen",
        "slug": "teen",
        "name_en": "Teen",
        "name_ru": "С 13 лет"
      },
      "user_game": null,
      "reviews_count": 31,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/bf5/bf5ac217a9e10ff98301e8359a870dc1.jpg"
        },
        {
          "id": 3071607,
          "image": "https://media.rawg.io/media/screenshots/07b/07bd2b444735ff613ef9f3673f70d619.jpg"
        },
        {
          "id": 3071608,
          "image": "https://media.rawg.io/media/screenshots/3c3/3c39189212ae9e7484792566ee5b68cb.jpg"
        },
        {
          "id": 3071609,
          "image": "https://media.rawg.io/media/screenshots/efb/efb4c72e1616a28093b334b943c0adb0.jpg"
        },
        {
          "id": 3071610,
          "image": "https://media.rawg.io/media/screenshots/32b/32b0b25f098418fef8a39bcbc2b7bfd6.jpg"
        },
        {
          "id": 3071611,
          "image": "https://media.rawg.io/media/screenshots/cd0/cd06199460fe631d767d5fab38172107.jpg"
        },
        {
          "id": 3071612,
          "image": "https://media.rawg.io/media/screenshots/5db/5db12a3d254beb433f753e4b478d3d03.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        }
      ],
      "genres": [
        {
          "id": 59,
          "name": "Massively Multiplayer",
          "slug": "massively-multiplayer"
        },
        {
          "id": 5,
          "name": "RPG",
          "slug": "role-playing-games-rpg"
        }
      ]
    },
    {
      "slug": "white-shadows",
      "name": "White Shadows",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-07",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/2e9/2e9b816222b52a615e0c04993282b777.jpg",
      "rating": 3.9,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 12,
          "percent": 60.0
        },
        {
          "id": 3,
          "title": "meh",
          "count": 5,
          "percent": 25.0
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 3,
          "percent": 15.0
        }
      ],
      "ratings_count": 20,
      "reviews_text_count": 0,
      "added": 96,
      "added_by_status": {
        "yet": 9,
        "owned": 44,
        "beaten": 14,
        "toplay": 26,
        "dropped": 3
      },
      "metacritic": 70,
      "suggestions_count": 240,
      "updated": "2023-05-10T06:26:47",
      "id": 663785,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205468,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34246,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29891,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30383,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45717,
          "image_background": "https://media.rawg.io/media/games/226/2262cea0b385db6cf399f4be831603b0.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14250,
          "image_background": "https://media.rawg.io/media/games/4cf/4cfc6b7f1850590a4634b08bfab308ab.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 29373,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 42401,
          "name": "Отличный саундтрек",
          "slug": "otlichnyi-saundtrek",
          "language": "rus",
          "games_count": 4459,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42,
          "name": "Great Soundtrack",
          "slug": "great-soundtrack",
          "language": "eng",
          "games_count": 3237,
          "image_background": "https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg"
        },
        {
          "id": 42394,
          "name": "Глубокий сюжет",
          "slug": "glubokii-siuzhet",
          "language": "rus",
          "games_count": 8926,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 18111,
          "image_background": "https://media.rawg.io/media/games/b8c/b8c243eaa0fbac8115e0cdccac3f91dc.jpg"
        },
        {
          "id": 42399,
          "name": "Казуальная игра",
          "slug": "kazualnaia-igra",
          "language": "rus",
          "games_count": 31428,
          "image_background": "https://media.rawg.io/media/games/37a/37a9536e92cf8fe3b60045aa75dbd41f.jpg"
        },
        {
          "id": 16,
          "name": "Horror",
          "slug": "horror",
          "language": "eng",
          "games_count": 41833,
          "image_background": "https://media.rawg.io/media/games/aa3/aa36ba4b486a03ddfaef274fb4f5afd4.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 10508,
          "image_background": "https://media.rawg.io/media/games/e3d/e3ddc524c6292a435d01d97cc5f42ea7.jpg"
        },
        {
          "id": 1,
          "name": "Survival",
          "slug": "survival",
          "language": "eng",
          "games_count": 7172,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 42452,
          "name": "Выживание",
          "slug": "vyzhivanie",
          "language": "rus",
          "games_count": 4663,
          "image_background": "https://media.rawg.io/media/games/7a4/7a45e4cdc5b07f316d49cf147b083b27.jpg"
        },
        {
          "id": 42463,
          "name": "Платформер",
          "slug": "platformer-2",
          "language": "rus",
          "games_count": 6444,
          "image_background": "https://media.rawg.io/media/screenshots/c97/c97b943741f5fbc936fe054d9d58851d.jpg"
        },
        {
          "id": 15,
          "name": "Stealth",
          "slug": "stealth",
          "language": "eng",
          "games_count": 5698,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 42439,
          "name": "Стелс",
          "slug": "stels",
          "language": "rus",
          "games_count": 1555,
          "image_background": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13412,
          "image_background": "https://media.rawg.io/media/games/9aa/9aa42d16d425fa6f179fc9dc2f763647.jpg"
        },
        {
          "id": 42477,
          "name": "Мрачная",
          "slug": "mrachnaia",
          "language": "rus",
          "games_count": 3551,
          "image_background": "https://media.rawg.io/media/games/d5a/d5a24f9f71315427fa6e966fdd98dfa6.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 14060,
          "image_background": "https://media.rawg.io/media/games/ebd/ebdbb7eb52bd58b0e7fa4538d9757b60.jpg"
        },
        {
          "id": 42556,
          "name": "Тайна",
          "slug": "taina",
          "language": "rus",
          "games_count": 3716,
          "image_background": "https://media.rawg.io/media/games/0af/0af85e8edddfa55368e47c539914a220.jpg"
        },
        {
          "id": 42469,
          "name": "Вид сбоку",
          "slug": "vid-sboku",
          "language": "rus",
          "games_count": 3015,
          "image_background": "https://media.rawg.io/media/screenshots/3f1/3f1c417b405a86ed7d92b903e0fcfd0c.jpg"
        },
        {
          "id": 117,
          "name": "Mystery",
          "slug": "mystery",
          "language": "eng",
          "games_count": 11819,
          "image_background": "https://media.rawg.io/media/games/5c0/5c0dd63002cb23f804aab327d40ef119.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5793,
          "image_background": "https://media.rawg.io/media/screenshots/2fc/2fc6994425146f9dba3133400b414e29.jpg"
        },
        {
          "id": 113,
          "name": "Side Scroller",
          "slug": "side-scroller",
          "language": "eng",
          "games_count": 9401,
          "image_background": "https://media.rawg.io/media/games/04a/04a7e7e185fb51493bdcbe1693a8b3dc.jpg"
        },
        {
          "id": 110,
          "name": "Cinematic",
          "slug": "cinematic",
          "language": "eng",
          "games_count": 1386,
          "image_background": "https://media.rawg.io/media/games/e2d/e2d3f396b16dded0f841c17c9799a882.jpg"
        },
        {
          "id": 119,
          "name": "Dystopian",
          "slug": "dystopian",
          "language": "eng",
          "games_count": 1774,
          "image_background": "https://media.rawg.io/media/games/157/15742f2f67eacff546738e1ab5c19d20.jpg"
        },
        {
          "id": 42665,
          "name": "Антиутопия",
          "slug": "antiutopiia",
          "language": "rus",
          "games_count": 850,
          "image_background": "https://media.rawg.io/media/games/7a2/7a2500ee8b2c0e1ff268bb4479463dea.jpg"
        },
        {
          "id": 42623,
          "name": "Кинематографичная",
          "slug": "kinematografichnaia",
          "language": "rus",
          "games_count": 1295,
          "image_background": "https://media.rawg.io/media/games/b51/b51c46cccb277e6451e2a67e4213a820.jpg"
        },
        {
          "id": 42672,
          "name": "Сюрреалистичная",
          "slug": "siurrealistichnaia",
          "language": "rus",
          "games_count": 1617,
          "image_background": "https://media.rawg.io/media/games/73e/73efc5c0ac6f354271dae610276f617c.jpg"
        },
        {
          "id": 46,
          "name": "Surreal",
          "slug": "surreal",
          "language": "eng",
          "games_count": 4515,
          "image_background": "https://media.rawg.io/media/screenshots/9d2/9d22689a1c2a7ced9d1690e0c5c66871.jpg"
        },
        {
          "id": 58132,
          "name": "Атмосферная",
          "slug": "atmosfernaia",
          "language": "rus",
          "games_count": 5000,
          "image_background": "https://media.rawg.io/media/games/5f1/5f1399f755ed3a40b04a9195f4c06be5.jpg"
        },
        {
          "id": 59643,
          "name": "Протагонистка",
          "slug": "protagonistka",
          "language": "eng",
          "games_count": 2978,
          "image_background": "https://media.rawg.io/media/games/2d8/2d84abb8b5e5ca1611048f682a61f93b.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 20,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/2e9/2e9b816222b52a615e0c04993282b777.jpg"
        },
        {
          "id": 3021218,
          "image": "https://media.rawg.io/media/screenshots/c73/c73e50ed0d417d69c152efefd63f8005.jpg"
        },
        {
          "id": 3021219,
          "image": "https://media.rawg.io/media/screenshots/22d/22d762b93ea8d368b5d0cc1bc49c9206.jpg"
        },
        {
          "id": 3021220,
          "image": "https://media.rawg.io/media/screenshots/e30/e3094d5c84b31e15d27d2db0bf01c480.jpg"
        },
        {
          "id": 3021221,
          "image": "https://media.rawg.io/media/screenshots/f6a/f6a16004e1e53d38922a3991982650af.jpg"
        },
        {
          "id": 3021222,
          "image": "https://media.rawg.io/media/screenshots/aec/aec6918c4a66fdc3f3872a215b330a75.jpg"
        },
        {
          "id": 3021223,
          "image": "https://media.rawg.io/media/screenshots/2e9/2e9b816222b52a615e0c04993282b777.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 83,
          "name": "Platformer",
          "slug": "platformer"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        }
      ]
    },
    {
      "slug": "heavenly-bodies-2",
      "name": "Heavenly Bodies",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-12-07",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/02e/02e25dae6cd7eaa0276eff1d2f95a9d1.jpg",
      "rating": 3.75,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 15,
          "percent": 41.67
        },
        {
          "id": 3,
          "title": "meh",
          "count": 11,
          "percent": 30.56
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 8,
          "percent": 22.22
        },
        {
          "id": 1,
          "title": "skip",
          "count": 2,
          "percent": 5.56
        }
      ],
      "ratings_count": 36,
      "reviews_text_count": 0,
      "added": 207,
      "added_by_status": {
        "yet": 18,
        "owned": 125,
        "beaten": 14,
        "toplay": 30,
        "dropped": 14,
        "playing": 6
      },
      "metacritic": 80,
      "suggestions_count": 181,
      "updated": "2023-04-30T18:03:41",
      "id": 364870,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 204994,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 31701,
          "image_background": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29695,
          "image_background": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14147,
          "image_background": "https://media.rawg.io/media/games/157/15742f2f67eacff546738e1ab5c19d20.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 9732,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 42413,
          "name": "Симулятор",
          "slug": "simuliator",
          "language": "rus",
          "games_count": 14875,
          "image_background": "https://media.rawg.io/media/games/59f/59fc1c5de1d29cb9234741c97d250150.jpg"
        },
        {
          "id": 75,
          "name": "Local Co-Op",
          "slug": "local-co-op",
          "language": "eng",
          "games_count": 4955,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 72,
          "name": "Local Multiplayer",
          "slug": "local-multiplayer",
          "language": "eng",
          "games_count": 12456,
          "image_background": "https://media.rawg.io/media/games/fc8/fc838d98c9b944e6a15176eabf40bee8.jpg"
        },
        {
          "id": 37796,
          "name": "exclusive",
          "slug": "exclusive",
          "language": "eng",
          "games_count": 4510,
          "image_background": "https://media.rawg.io/media/games/d64/d646810b629081cc12aec49ed9f49441.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 36,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/02e/02e25dae6cd7eaa0276eff1d2f95a9d1.jpg"
        },
        {
          "id": 2129811,
          "image": "https://media.rawg.io/media/screenshots/971/9711c0e75e468095813510ff2bf1224a.jpg"
        },
        {
          "id": 2129812,
          "image": "https://media.rawg.io/media/screenshots/52f/52fb8f879763c4f5a6eadc359f1fcb97.jpg"
        },
        {
          "id": 2129813,
          "image": "https://media.rawg.io/media/screenshots/4f1/4f1a613efb3981c4d55c11b65ecd056a.jpg"
        },
        {
          "id": 2129814,
          "image": "https://media.rawg.io/media/screenshots/a08/a08a1fa01428a7dd7d90ceac9444123d.jpg"
        },
        {
          "id": 2129815,
          "image": "https://media.rawg.io/media/screenshots/a0a/a0ac7a200a94a00ba78f6e073e6dc3ad.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        },
        {
          "id": 14,
          "name": "Simulation",
          "slug": "simulation"
        }
      ]
    },
    {
      "slug": "the-matrix-awakens",
      "name": "The Matrix Awakens",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-12-05",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/ca6/ca6d2ad4c573828ac4bad3f3cd6cf6f5.jpg",
      "rating": 4.15,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 20,
          "percent": 60.61
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 9,
          "percent": 27.27
        },
        {
          "id": 3,
          "title": "meh",
          "count": 4,
          "percent": 12.12
        }
      ],
      "ratings_count": 33,
      "reviews_text_count": 0,
      "added": 85,
      "added_by_status": {
        "yet": 10,
        "owned": 15,
        "beaten": 35,
        "toplay": 14,
        "dropped": 11
      },
      "metacritic": null,
      "suggestions_count": 419,
      "updated": "2023-04-13T21:03:05",
      "id": 704811,
      "score": null,
      "clip": null,
      "tags": [],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 33,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/ca6/ca6d2ad4c573828ac4bad3f3cd6cf6f5.jpg"
        },
        {
          "id": 3195693,
          "image": "https://media.rawg.io/media/screenshots/db7/db770b4e31a4d6346ba656668b212559.jpg"
        },
        {
          "id": 3195694,
          "image": "https://media.rawg.io/media/screenshots/348/34895b56d153ded7a654724891368095.jpg"
        },
        {
          "id": 3195695,
          "image": "https://media.rawg.io/media/screenshots/ede/ede5da6c8717fb5ab03c903da7ecc694.jpg"
        },
        {
          "id": 3195696,
          "image": "https://media.rawg.io/media/screenshots/b5c/b5c0c3087d2ab2dd7a4309f8ff82bc35.jpg"
        },
        {
          "id": 3195697,
          "image": "https://media.rawg.io/media/screenshots/e0b/e0bae711a85ac76d2f9b7ec9f25fb444.jpg"
        },
        {
          "id": 3195698,
          "image": "https://media.rawg.io/media/screenshots/1db/1dbcd0df4d6c4df0e6e7a0119786b381_42heSXY.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter"
        }
      ]
    },
    {
      "slug": "aftermath-2022",
      "name": "Aftermath (2022)",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        }
      ],
      "released": "2021-12-05",
      "tba": true,
      "background_image": "https://media.rawg.io/media/screenshots/b34/b3468c2c1e3baa3e7210b80ca9fcbf2c.jpg",
      "rating": 0.0,
      "rating_top": 0,
      "ratings": [],
      "ratings_count": 0,
      "reviews_text_count": 0,
      "added": 7,
      "added_by_status": {
        "owned": 2,
        "toplay": 5
      },
      "metacritic": null,
      "suggestions_count": 48,
      "updated": "2021-12-09T20:58:18",
      "id": 704636,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 202950,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 31207,
          "image_background": "https://media.rawg.io/media/games/4a0/4a0a1316102366260e6f38fd2a9cfdce.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 30381,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 44069,
          "image_background": "https://media.rawg.io/media/games/b6b/b6b20bfc4b34e312dbc8aac53c95a348.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 28254,
          "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
        },
        {
          "id": 42399,
          "name": "Казуальная игра",
          "slug": "kazualnaia-igra",
          "language": "rus",
          "games_count": 29526,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 45,
          "name": "2D",
          "slug": "2d",
          "language": "eng",
          "games_count": 192201,
          "image_background": "https://media.rawg.io/media/games/9fa/9fa63622543e5d4f6d99aa9d73b043de.jpg"
        },
        {
          "id": 40845,
          "name": "Partial Controller Support",
          "slug": "partial-controller-support",
          "language": "eng",
          "games_count": 9314,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 42463,
          "name": "Платформер",
          "slug": "platformer-2",
          "language": "rus",
          "games_count": 6036,
          "image_background": "https://media.rawg.io/media/games/942/9424d6bb763dc38d9378b488603c87fa.jpg"
        },
        {
          "id": 42467,
          "name": "Ретро",
          "slug": "retro-2",
          "language": "rus",
          "games_count": 5252,
          "image_background": "https://media.rawg.io/media/games/218/218167ff4011acc825c844d0070940a0.jpg"
        },
        {
          "id": 42411,
          "name": "Ранний доступ",
          "slug": "rannii-dostup",
          "language": "rus",
          "games_count": 11071,
          "image_background": "https://media.rawg.io/media/games/35b/35b47c4d85cd6e08f3e2ca43ea5ce7bb.jpg"
        },
        {
          "id": 42477,
          "name": "Мрачная",
          "slug": "mrachnaia",
          "language": "rus",
          "games_count": 3230,
          "image_background": "https://media.rawg.io/media/games/63f/63f0e68688cad279ed38cde931dbfcdb.jpg"
        },
        {
          "id": 74,
          "name": "Retro",
          "slug": "retro",
          "language": "eng",
          "games_count": 39561,
          "image_background": "https://media.rawg.io/media/games/713/713269608dc8f2f40f5a670a14b2de94.jpg"
        },
        {
          "id": 41,
          "name": "Dark",
          "slug": "dark",
          "language": "eng",
          "games_count": 13686,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        },
        {
          "id": 14,
          "name": "Early Access",
          "slug": "early-access",
          "language": "eng",
          "games_count": 11552,
          "image_background": "https://media.rawg.io/media/screenshots/848/848253347dc93c762bfd51c7e4989b8f.jpg"
        },
        {
          "id": 42469,
          "name": "Вид сбоку",
          "slug": "vid-sboku",
          "language": "rus",
          "games_count": 2764,
          "image_background": "https://media.rawg.io/media/games/23a/23acbd56da0c30bca0227967a5720c96.jpg"
        },
        {
          "id": 113,
          "name": "Side Scroller",
          "slug": "side-scroller",
          "language": "eng",
          "games_count": 9182,
          "image_background": "https://media.rawg.io/media/games/c40/c40f9f0a3d1b4601a7a44d230c95f126.jpg"
        },
        {
          "id": 56,
          "name": "Shoot 'Em Up",
          "slug": "shoot-em-up",
          "language": "eng",
          "games_count": 10677,
          "image_background": "https://media.rawg.io/media/screenshots/404/404eac84f1bb799e5d7d62cca9cf836a.jpg"
        },
        {
          "id": 305,
          "name": "Linear",
          "slug": "linear",
          "language": "eng",
          "games_count": 3394,
          "image_background": "https://media.rawg.io/media/games/106/1069e754e7e6012b0cf42b4b04704792.jpg"
        },
        {
          "id": 42437,
          "name": "Линейная",
          "slug": "lineinaia",
          "language": "rus",
          "games_count": 3337,
          "image_background": "https://media.rawg.io/media/games/2ba/2ba37c3516b73e4b67bb3c1a69bb6478.jpg"
        },
        {
          "id": 42582,
          "name": "Минимализм",
          "slug": "minimalizm",
          "language": "rus",
          "games_count": 3342,
          "image_background": "https://media.rawg.io/media/screenshots/5a3/5a335ee04477c97d393e8ba03f8e6d91.jpg"
        },
        {
          "id": 187,
          "name": "Demons",
          "slug": "demons",
          "language": "eng",
          "games_count": 1854,
          "image_background": "https://media.rawg.io/media/games/0fa/0fa9e2b8397b332902d3b56c73888d52.jpg"
        },
        {
          "id": 179,
          "name": "Cartoon",
          "slug": "cartoon",
          "language": "eng",
          "games_count": 4301,
          "image_background": "https://media.rawg.io/media/games/aac/aac683272f862540a18625f02f5f3679.jpg"
        },
        {
          "id": 215,
          "name": "Dragons",
          "slug": "dragons",
          "language": "eng",
          "games_count": 2374,
          "image_background": "https://media.rawg.io/media/screenshots/755/7557715b92e3db2914e73c4fccfa7fdc.jpg"
        },
        {
          "id": 42545,
          "name": "Демоны",
          "slug": "demony",
          "language": "rus",
          "games_count": 1013,
          "image_background": "https://media.rawg.io/media/games/3b0/3b01313965c19adc6b6c37a3d9d33576.jpg"
        },
        {
          "id": 112,
          "name": "Minimalist",
          "slug": "minimalist",
          "language": "eng",
          "games_count": 13720,
          "image_background": "https://media.rawg.io/media/games/c49/c4983df94a8a8167d652b812de742da8.jpg"
        },
        {
          "id": 42614,
          "name": "Драконы",
          "slug": "drakony",
          "language": "rus",
          "games_count": 454,
          "image_background": "https://media.rawg.io/media/games/dbb/dbba6100aae179b5f24052c9141d426d.jpg"
        },
        {
          "id": 58132,
          "name": "Атмосферная",
          "slug": "atmosfernaia",
          "language": "rus",
          "games_count": 4117,
          "image_background": "https://media.rawg.io/media/screenshots/30f/30fe09ca354adfd72aeb47b34dbf153a.jpg"
        },
        {
          "id": 295,
          "name": "Soundtrack",
          "slug": "soundtrack",
          "language": "eng",
          "games_count": 2713,
          "image_background": "https://media.rawg.io/media/screenshots/e9e/e9e13e0a1b9e6d95d438f6d58c99a440.jpg"
        },
        {
          "id": 49959,
          "name": "2D Platformer",
          "slug": "2d-platformer-3",
          "language": "eng",
          "games_count": 3682,
          "image_background": "https://media.rawg.io/media/games/a53/a53575214f12dadb9f6d7dca9b24cda4.jpg"
        },
        {
          "id": 49995,
          "name": "2D-платформер",
          "slug": "2d-platformer-4",
          "language": "eng",
          "games_count": 3638,
          "image_background": "https://media.rawg.io/media/games/fd4/fd4e41d4e83714ff892702895cf4af08.jpg"
        },
        {
          "id": 42698,
          "name": "Саундтрек",
          "slug": "saundtrek",
          "language": "rus",
          "games_count": 384,
          "image_background": "https://media.rawg.io/media/screenshots/65b/65b9b6c96f093df897d14844e7b60819_bcrhbi4.jpg"
        },
        {
          "id": 49963,
          "name": "Precision Platformer",
          "slug": "precision-platformer-2",
          "language": "eng",
          "games_count": 1078,
          "image_background": "https://media.rawg.io/media/screenshots/538/5382b1da5fa87f23635d18c2ecc364ce.jpg"
        },
        {
          "id": 70352,
          "name": "Мультфильмы",
          "slug": "multfilmy",
          "language": "rus",
          "games_count": 1125,
          "image_background": "https://media.rawg.io/media/screenshots/b3d/b3df40065f51627e1f7fdc70135317c7_DHhrVjl.jpg"
        },
        {
          "id": 49999,
          "name": "Платформер на точность",
          "slug": "platformer-na-tochnost",
          "language": "rus",
          "games_count": 1065,
          "image_background": "https://media.rawg.io/media/games/120/120fb9bd3078ba475e525f592960592f.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 0,
      "community_rating": 0,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/b34/b3468c2c1e3baa3e7210b80ca9fcbf2c.jpg"
        },
        {
          "id": 3139451,
          "image": "https://media.rawg.io/media/screenshots/b34/b3468c2c1e3baa3e7210b80ca9fcbf2c.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 40,
          "name": "Casual",
          "slug": "casual"
        },
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "happys-humble-burger-farm",
      "name": "Happy's Humble Burger Farm",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        },
        {
          "store": {
            "id": 2,
            "name": "Xbox Store",
            "slug": "xbox-store"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-02",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/c7f/c7fc74944d38615b9b4049cda0645c33.jpg",
      "rating": 3.45,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 3,
          "percent": 27.27
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 3,
          "percent": 27.27
        },
        {
          "id": 3,
          "title": "meh",
          "count": 3,
          "percent": 27.27
        },
        {
          "id": 1,
          "title": "skip",
          "count": 2,
          "percent": 18.18
        }
      ],
      "ratings_count": 11,
      "reviews_text_count": 0,
      "added": 59,
      "added_by_status": {
        "yet": 1,
        "owned": 44,
        "beaten": 8,
        "toplay": 3,
        "dropped": 3
      },
      "metacritic": null,
      "suggestions_count": 147,
      "updated": "2023-01-29T21:42:29",
      "id": 702250,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 204815,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 31630,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 29625,
          "image_background": "https://media.rawg.io/media/games/4a0/4a0a1316102366260e6f38fd2a9cfdce.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30112,
          "image_background": "https://media.rawg.io/media/games/49c/49c3dfa4ce2f6f140cc4825868e858cb.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14125,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 29221,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 28344,
          "image_background": "https://media.rawg.io/media/games/b54/b54598d1d5cc31899f4f0a7e3122a7b0.jpg"
        },
        {
          "id": 42429,
          "name": "От первого лица",
          "slug": "ot-pervogo-litsa",
          "language": "rus",
          "games_count": 7388,
          "image_background": "https://media.rawg.io/media/games/b7b/b7b8381707152afc7d91f5d95de70e39.jpg"
        },
        {
          "id": 16,
          "name": "Horror",
          "slug": "horror",
          "language": "eng",
          "games_count": 41752,
          "image_background": "https://media.rawg.io/media/games/2ad/2ad87a4a69b1104f02435c14c5196095.jpg"
        },
        {
          "id": 1,
          "name": "Survival",
          "slug": "survival",
          "language": "eng",
          "games_count": 7113,
          "image_background": "https://media.rawg.io/media/games/7f6/7f6cd70ba2ad57053b4847c13569f2d8.jpg"
        },
        {
          "id": 42452,
          "name": "Выживание",
          "slug": "vyzhivanie",
          "language": "rus",
          "games_count": 4605,
          "image_background": "https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg"
        },
        {
          "id": 42415,
          "name": "Пиксельная графика",
          "slug": "pikselnaia-grafika",
          "language": "rus",
          "games_count": 8724,
          "image_background": "https://media.rawg.io/media/games/ffe/ffed87105b14f5beff72ff44a7793fd5.jpg"
        },
        {
          "id": 122,
          "name": "Pixel Graphics",
          "slug": "pixel-graphics",
          "language": "eng",
          "games_count": 89937,
          "image_background": "https://media.rawg.io/media/games/f46/f466571d536f2e3ea9e815ad17177501.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13301,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42471,
          "name": "Хоррор на выживание",
          "slug": "khorror-na-vyzhivanie",
          "language": "rus",
          "games_count": 2161,
          "image_background": "https://media.rawg.io/media/games/e6d/e6de699bd788497f4b52e2f41f9698f2.jpg"
        },
        {
          "id": 17,
          "name": "Survival Horror",
          "slug": "survival-horror",
          "language": "eng",
          "games_count": 7534,
          "image_background": "https://media.rawg.io/media/games/0b3/0b34647c42271600399b93d19b10f1aa.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5680,
          "image_background": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 42460,
          "name": "Реализм",
          "slug": "realizm",
          "language": "rus",
          "games_count": 3706,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 77,
          "name": "Realistic",
          "slug": "realistic",
          "language": "eng",
          "games_count": 3731,
          "image_background": "https://media.rawg.io/media/screenshots/f34/f34c86335d0c51baa582aa93fa2d3f55.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 76946,
          "image_background": "https://media.rawg.io/media/games/4be/4be6a6ad0364751a96229c56bf69be59.jpg"
        },
        {
          "id": 42551,
          "name": "Менеджмент",
          "slug": "menedzhment",
          "language": "rus",
          "games_count": 2535,
          "image_background": "https://media.rawg.io/media/games/25c/25c4776ab5723d5d735d8bf617ca12d9.jpg"
        },
        {
          "id": 119,
          "name": "Dystopian",
          "slug": "dystopian",
          "language": "eng",
          "games_count": 1755,
          "image_background": "https://media.rawg.io/media/games/2e1/2e187b31e5cee21c110bd16798d75fab.jpg"
        },
        {
          "id": 42665,
          "name": "Антиутопия",
          "slug": "antiutopiia",
          "language": "rus",
          "games_count": 831,
          "image_background": "https://media.rawg.io/media/games/8e4/8e4de3f54ac659e08a7ba6a2b731682a.jpg"
        },
        {
          "id": 42484,
          "name": "90-е",
          "slug": "90-e",
          "language": "rus",
          "games_count": 1730,
          "image_background": "https://media.rawg.io/media/screenshots/656/65654f69256420c0126eb506c1a72d7f.jpg"
        },
        {
          "id": 42512,
          "name": "Олдскул",
          "slug": "oldskul",
          "language": "rus",
          "games_count": 2542,
          "image_background": "https://media.rawg.io/media/screenshots/010/0101f021b2dc123c98969fda7e4bcd92.jpg"
        },
        {
          "id": 67,
          "name": "Management",
          "slug": "management",
          "language": "eng",
          "games_count": 6447,
          "image_background": "https://media.rawg.io/media/screenshots/0bf/0bfa18254ae627cab7f0159a24d178cd.jpg"
        },
        {
          "id": 40937,
          "name": "Steam Trading Cards",
          "slug": "steam-trading-cards-2",
          "language": "eng",
          "games_count": 403,
          "image_background": "https://media.rawg.io/media/games/370/3703c683968a54f09630dcf03366ea35.jpg"
        },
        {
          "id": 42502,
          "name": "Чёрный юмор",
          "slug": "chiornyi-iumor",
          "language": "rus",
          "games_count": 1144,
          "image_background": "https://media.rawg.io/media/games/2a2/2a2f9a0035544500e59a171c7038ec3a.jpg"
        },
        {
          "id": 243,
          "name": "1990's",
          "slug": "1990s",
          "language": "eng",
          "games_count": 1634,
          "image_background": "https://media.rawg.io/media/games/abd/abdb7e589f0a8ccd36c0582673120b1e.jpg"
        },
        {
          "id": 148,
          "name": "Dark Humor",
          "slug": "dark-humor",
          "language": "eng",
          "games_count": 2431,
          "image_background": "https://media.rawg.io/media/games/c2e/c2e6ad5c838d551aeff376f1f3d9d65e.jpg"
        },
        {
          "id": 205,
          "name": "Lore-Rich",
          "slug": "lore-rich",
          "language": "eng",
          "games_count": 773,
          "image_background": "https://media.rawg.io/media/screenshots/c6e/c6eb418beab0a064347be8b890dea33f.jpeg"
        },
        {
          "id": 42594,
          "name": "Проработанная вселенная",
          "slug": "prorabotannaia-vselennaia",
          "language": "rus",
          "games_count": 800,
          "image_background": "https://media.rawg.io/media/games/c24/c24f4434882ae9c2c8d9d38de82cb7a5.jpg"
        },
        {
          "id": 58132,
          "name": "Атмосферная",
          "slug": "atmosfernaia",
          "language": "rus",
          "games_count": 4850,
          "image_background": "https://media.rawg.io/media/screenshots/d0f/d0f3e325366eeb8912b0f8752ed97606.jpg"
        },
        {
          "id": 42473,
          "name": "Immersive Sim",
          "slug": "immersive-sim-2",
          "language": "eng",
          "games_count": 1653,
          "image_background": "https://media.rawg.io/media/games/6ee/6ee5cbe30c6b6c299a3288337d40a0f3.jpg"
        },
        {
          "id": 291,
          "name": "Conspiracy",
          "slug": "conspiracy",
          "language": "eng",
          "games_count": 595,
          "image_background": "https://media.rawg.io/media/games/4b1/4b11e8fc75e054939c29e7319a643600.jpg"
        },
        {
          "id": 42641,
          "name": "Заговор",
          "slug": "zagovor",
          "language": "rus",
          "games_count": 363,
          "image_background": "https://media.rawg.io/media/screenshots/5f0/5f00e2338ab8fa6c48d05d4a2bb9dc60.jpg"
        },
        {
          "id": 43374,
          "name": "Remote Play on TV",
          "slug": "remote-play-on-tv",
          "language": "eng",
          "games_count": 315,
          "image_background": "https://media.rawg.io/media/games/538/538b320977e1075eb279e381e6b92cc8.jpg"
        },
        {
          "id": 43578,
          "name": "Remote Play on Tablet",
          "slug": "remote-play-on-tablet",
          "language": "eng",
          "games_count": 146,
          "image_background": "https://media.rawg.io/media/screenshots/064/06454359900963f2d95e7ff1135c79f1.jpg"
        },
        {
          "id": 570,
          "name": "Old School",
          "slug": "old-school",
          "language": "eng",
          "games_count": 2295,
          "image_background": "https://media.rawg.io/media/games/0d4/0d4ab33c89c9772db39ab67183a4a8f5.jpg"
        },
        {
          "id": 43577,
          "name": "Remote Play on Phone",
          "slug": "remote-play-on-phone",
          "language": "eng",
          "games_count": 141,
          "image_background": "https://media.rawg.io/media/screenshots/c96/c96a73cc5e6ca7e99b0142f95a7d2b7f.jpg"
        },
        {
          "id": 42509,
          "name": "Тайм-менеджмент",
          "slug": "taim-menedzhment",
          "language": "rus",
          "games_count": 1111,
          "image_background": "https://media.rawg.io/media/screenshots/cb7/cb792454f589ba16eaf89ff632cd5ad5.jpg"
        },
        {
          "id": 58125,
          "name": "Иммерсивный симулятор",
          "slug": "immersivnyi-simuliator",
          "language": "rus",
          "games_count": 1525,
          "image_background": "https://media.rawg.io/media/games/018/01857c5ff9579c48fa8bd76b4d83a946.jpg"
        },
        {
          "id": 568,
          "name": "Time Management",
          "slug": "time-management",
          "language": "eng",
          "games_count": 949,
          "image_background": "https://media.rawg.io/media/screenshots/006/006aeb072314f08590e7ab6b38e7721b.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 11,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/c7f/c7fc74944d38615b9b4049cda0645c33.jpg"
        },
        {
          "id": 3132354,
          "image": "https://media.rawg.io/media/screenshots/f28/f28ae392f4c32bc8d751196ff3cab6eb.jpg"
        },
        {
          "id": 3132355,
          "image": "https://media.rawg.io/media/screenshots/ee2/ee23379fb7d9cdf73982d22a433ea41b.jpg"
        },
        {
          "id": 3132356,
          "image": "https://media.rawg.io/media/screenshots/cc4/cc4c5bc34a6c788ba3e65a24b17ef716.jpg"
        },
        {
          "id": 3132357,
          "image": "https://media.rawg.io/media/screenshots/5fa/5fa44c137783d16198569f6d18eae922.jpg"
        },
        {
          "id": 3132358,
          "image": "https://media.rawg.io/media/screenshots/c7f/c7fc74944d38615b9b4049cda0645c33.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        },
        {
          "id": 14,
          "name": "Simulation",
          "slug": "simulation"
        }
      ]
    },
    {
      "slug": "solar-ash-kingdom",
      "name": "Solar Ash",
      "playtime": 1,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-02",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/e84/e844fe6d2f7182096b1e9f214e511ca6.jpg",
      "rating": 3.93,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 23,
          "percent": 52.27
        },
        {
          "id": 3,
          "title": "meh",
          "count": 12,
          "percent": 27.27
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 9,
          "percent": 20.45
        }
      ],
      "ratings_count": 42,
      "reviews_text_count": 2,
      "added": 317,
      "added_by_status": {
        "yet": 39,
        "owned": 59,
        "beaten": 29,
        "toplay": 164,
        "dropped": 22,
        "playing": 4
      },
      "metacritic": 78,
      "suggestions_count": 47,
      "updated": "2023-06-05T13:41:52",
      "id": 301514,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205673,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34451,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 32049,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 30012,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30493,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14299,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 13,
          "name": "Atmospheric",
          "slug": "atmospheric",
          "language": "eng",
          "games_count": 29423,
          "image_background": "https://media.rawg.io/media/games/490/49016e06ae2103881ff6373248843069.jpg"
        },
        {
          "id": 40849,
          "name": "Steam Cloud",
          "slug": "steam-cloud",
          "language": "eng",
          "games_count": 14157,
          "image_background": "https://media.rawg.io/media/games/f46/f466571d536f2e3ea9e815ad17177501.jpg"
        },
        {
          "id": 42394,
          "name": "Глубокий сюжет",
          "slug": "glubokii-siuzhet",
          "language": "rus",
          "games_count": 8979,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 118,
          "name": "Story Rich",
          "slug": "story-rich",
          "language": "eng",
          "games_count": 18160,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42442,
          "name": "Открытый мир",
          "slug": "otkrytyi-mir",
          "language": "rus",
          "games_count": 4432,
          "image_background": "https://media.rawg.io/media/games/d69/d69810315bd7e226ea2d21f9156af629.jpg"
        },
        {
          "id": 36,
          "name": "Open World",
          "slug": "open-world",
          "language": "eng",
          "games_count": 6255,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 9401,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42441,
          "name": "От третьего лица",
          "slug": "ot-tretego-litsa",
          "language": "rus",
          "games_count": 4912,
          "image_background": "https://media.rawg.io/media/games/d1a/d1a2e99ade53494c6330a0ed945fe823.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 17134,
          "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
        },
        {
          "id": 42423,
          "name": "Научная фантастика",
          "slug": "nauchnaia-fantastika",
          "language": "rus",
          "games_count": 6004,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 6,
          "name": "Exploration",
          "slug": "exploration",
          "language": "eng",
          "games_count": 19339,
          "image_background": "https://media.rawg.io/media/games/e6d/e6de699bd788497f4b52e2f41f9698f2.jpg"
        },
        {
          "id": 37,
          "name": "Sandbox",
          "slug": "sandbox",
          "language": "eng",
          "games_count": 5892,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 42444,
          "name": "Песочница",
          "slug": "pesochnitsa",
          "language": "rus",
          "games_count": 3128,
          "image_background": "https://media.rawg.io/media/games/960/960b601d9541cec776c5fa42a00bf6c4.jpg"
        },
        {
          "id": 42463,
          "name": "Платформер",
          "slug": "platformer-2",
          "language": "rus",
          "games_count": 6485,
          "image_background": "https://media.rawg.io/media/games/9cc/9cc11e2e81403186c7fa9c00c143d6e4.jpg"
        },
        {
          "id": 42416,
          "name": "Контроллер",
          "slug": "kontroller",
          "language": "rus",
          "games_count": 3865,
          "image_background": "https://media.rawg.io/media/games/410/41033a495ce8f7fd4b0934bdb975f12a.jpg"
        },
        {
          "id": 115,
          "name": "Controller",
          "slug": "controller",
          "language": "eng",
          "games_count": 9407,
          "image_background": "https://media.rawg.io/media/games/fc3/fc30790a3b3c738d7a271b02c1e26dc2.jpg"
        },
        {
          "id": 69,
          "name": "Action-Adventure",
          "slug": "action-adventure",
          "language": "eng",
          "games_count": 13460,
          "image_background": "https://media.rawg.io/media/games/9aa/9aa42d16d425fa6f179fc9dc2f763647.jpg"
        },
        {
          "id": 25,
          "name": "Space",
          "slug": "space",
          "language": "eng",
          "games_count": 41753,
          "image_background": "https://media.rawg.io/media/screenshots/bf7/bf71c819eace914e6c42ae3ecb667308.jpg"
        },
        {
          "id": 42487,
          "name": "Слэшер",
          "slug": "slesher",
          "language": "rus",
          "games_count": 1947,
          "image_background": "https://media.rawg.io/media/games/5be/5bec14622f6faf804a592176577c1347.jpg"
        },
        {
          "id": 42422,
          "name": "Космос",
          "slug": "kosmos-2",
          "language": "rus",
          "games_count": 3137,
          "image_background": "https://media.rawg.io/media/games/65f/65f661f1487395055ba05e0a95fc0330.jpg"
        },
        {
          "id": 68,
          "name": "Hack and Slash",
          "slug": "hack-and-slash",
          "language": "eng",
          "games_count": 3431,
          "image_background": "https://media.rawg.io/media/games/1fb/1fb1c5f7a71d771f440b27ce7f71e7eb.jpg"
        },
        {
          "id": 37796,
          "name": "exclusive",
          "slug": "exclusive",
          "language": "eng",
          "games_count": 4506,
          "image_background": "https://media.rawg.io/media/games/cd3/cd3c9c7d3e95cb1608fd6250f1b90b7a.jpg"
        },
        {
          "id": 42601,
          "name": "Цветастая",
          "slug": "tsvetastaia",
          "language": "rus",
          "games_count": 9202,
          "image_background": "https://media.rawg.io/media/games/0be/0bea0a08a4d954337305391b778a7f37.jpg"
        },
        {
          "id": 117,
          "name": "Mystery",
          "slug": "mystery",
          "language": "eng",
          "games_count": 11847,
          "image_background": "https://media.rawg.io/media/games/0be/0bea0a08a4d954337305391b778a7f37.jpg"
        },
        {
          "id": 42490,
          "name": "Приключенческий экшен",
          "slug": "prikliuchencheskii-ekshen",
          "language": "rus",
          "games_count": 5844,
          "image_background": "https://media.rawg.io/media/games/d1c/d1cd8a226cb224357c1f59605577cbf2.jpg"
        },
        {
          "id": 167,
          "name": "Futuristic",
          "slug": "futuristic",
          "language": "eng",
          "games_count": 4318,
          "image_background": "https://media.rawg.io/media/games/c24/c24ec439abf4a2e92f3429dfa83f7f94.jpg"
        },
        {
          "id": 165,
          "name": "Colorful",
          "slug": "colorful",
          "language": "eng",
          "games_count": 17780,
          "image_background": "https://media.rawg.io/media/games/8e4/8e4de3f54ac659e08a7ba6a2b731682a.jpg"
        },
        {
          "id": 42451,
          "name": "Будущее",
          "slug": "budushchee",
          "language": "rus",
          "games_count": 2449,
          "image_background": "https://media.rawg.io/media/games/a12/a12f806432cb385bc286f0935c49cd14.jpg"
        },
        {
          "id": 1465,
          "name": "combat",
          "slug": "combat",
          "language": "eng",
          "games_count": 9361,
          "image_background": "https://media.rawg.io/media/games/315/3156817d3ac1f341da73de6495fb28f5.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 77232,
          "image_background": "https://media.rawg.io/media/games/74c/74c68a8de3d4983ff932dd456ac2dc66.jpg"
        },
        {
          "id": 42494,
          "name": "3D-платформер",
          "slug": "3d-platformer-2",
          "language": "rus",
          "games_count": 2695,
          "image_background": "https://media.rawg.io/media/games/fd7/fd794a9f0ffe816038d981b3acc3eec9.jpg"
        },
        {
          "id": 229,
          "name": "3D Platformer",
          "slug": "3d-platformer",
          "language": "eng",
          "games_count": 8728,
          "image_background": "https://media.rawg.io/media/games/444/4440f674e2bcb257e249a9ab595d8ab6.jpg"
        },
        {
          "id": 58132,
          "name": "Атмосферная",
          "slug": "atmosfernaia",
          "language": "rus",
          "games_count": 5055,
          "image_background": "https://media.rawg.io/media/games/e85/e851f527ab0658519436342ee73da191.jpg"
        },
        {
          "id": 58127,
          "name": "Бой",
          "slug": "boi-2",
          "language": "rus",
          "games_count": 4294,
          "image_background": "https://media.rawg.io/media/games/e35/e355efef97722d50bec3482a2263b962.jpg"
        },
        {
          "id": 66533,
          "name": "Исследования",
          "slug": "issledovaniia",
          "language": "rus",
          "games_count": 4665,
          "image_background": "https://media.rawg.io/media/games/91d/91ddeef8d5ebee7f21faa89efa0f2201.jpg"
        },
        {
          "id": 3758,
          "name": "speed",
          "slug": "speed",
          "language": "eng",
          "games_count": 8062,
          "image_background": "https://media.rawg.io/media/games/cb5/cb524d6981f187e3558527a7b7a3f7c2.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 44,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/e84/e844fe6d2f7182096b1e9f214e511ca6.jpg"
        },
        {
          "id": 1868457,
          "image": "https://media.rawg.io/media/screenshots/164/164ce500a653d3261be766afe2385852.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "chorus",
      "name": "Chorus",
      "playtime": 3,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-12-02",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/656/656bf6751565fe441af18db27be92b12.jpg",
      "rating": 3.57,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 35,
          "percent": 46.67
        },
        {
          "id": 3,
          "title": "meh",
          "count": 24,
          "percent": 32.0
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 10,
          "percent": 13.33
        },
        {
          "id": 1,
          "title": "skip",
          "count": 6,
          "percent": 8.0
        }
      ],
      "ratings_count": 72,
      "reviews_text_count": 3,
      "added": 648,
      "added_by_status": {
        "yet": 39,
        "owned": 452,
        "beaten": 30,
        "toplay": 72,
        "dropped": 45,
        "playing": 10
      },
      "metacritic": 76,
      "suggestions_count": 237,
      "updated": "2023-06-10T20:59:25",
      "id": 440084,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205673,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 34451,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 32049,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42392,
          "name": "Приключение",
          "slug": "prikliuchenie",
          "language": "rus",
          "games_count": 30012,
          "image_background": "https://media.rawg.io/media/games/562/562553814dd54e001a541e4ee83a591c.jpg"
        },
        {
          "id": 42398,
          "name": "Инди",
          "slug": "indi-2",
          "language": "rus",
          "games_count": 45833,
          "image_background": "https://media.rawg.io/media/games/93e/93ee6101e1c943732f06080dddb0fe4c.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14299,
          "image_background": "https://media.rawg.io/media/games/8a0/8a02f84a5916ede2f923b88d5f8217ba.jpg"
        },
        {
          "id": 42428,
          "name": "Шутер",
          "slug": "shuter",
          "language": "rus",
          "games_count": 6722,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 17134,
          "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
        },
        {
          "id": 42423,
          "name": "Научная фантастика",
          "slug": "nauchnaia-fantastika",
          "language": "rus",
          "games_count": 6004,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 189,
          "name": "Female Protagonist",
          "slug": "female-protagonist",
          "language": "eng",
          "games_count": 10532,
          "image_background": "https://media.rawg.io/media/games/62c/62c7c8b28a27b83680b22fb9d33fc619.jpg"
        },
        {
          "id": 59643,
          "name": "Протагонистка",
          "slug": "protagonistka",
          "language": "eng",
          "games_count": 3002,
          "image_background": "https://media.rawg.io/media/games/91d/91ddeef8d5ebee7f21faa89efa0f2201.jpg"
        }
      ],
      "esrb_rating": {
        "id": 3,
        "name": "Teen",
        "slug": "teen",
        "name_en": "Teen",
        "name_ru": "С 13 лет"
      },
      "user_game": null,
      "reviews_count": 75,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/656/656bf6751565fe441af18db27be92b12.jpg"
        },
        {
          "id": 2374848,
          "image": "https://media.rawg.io/media/screenshots/795/795b3f8dca10dfc4e479bb9142e5a661.jpg"
        },
        {
          "id": 2374849,
          "image": "https://media.rawg.io/media/screenshots/83b/83b0a637e28fa2c39d02dac408a515c1.jpg"
        },
        {
          "id": 2374850,
          "image": "https://media.rawg.io/media/screenshots/386/386675cb4c3a3aec2217c539c4543cf3.jpg"
        },
        {
          "id": 2374851,
          "image": "https://media.rawg.io/media/screenshots/86b/86bbd204728a795902bb40197a32079b.jpg"
        },
        {
          "id": 2374852,
          "image": "https://media.rawg.io/media/screenshots/411/411bc6803f8dfaacefc5f36b4215beb6.jpg"
        },
        {
          "id": 2374853,
          "image": "https://media.rawg.io/media/screenshots/896/89627a1d6831dac1f2caaffa2af37e14.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        }
      ],
      "genres": [
        {
          "id": 51,
          "name": "Indie",
          "slug": "indie"
        },
        {
          "id": 2,
          "name": "Shooter",
          "slug": "shooter"
        },
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        },
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "dead-by-daylight-portrait-of-a-murder",
      "name": "Dead by Daylight - Portrait of a Murder",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        }
      ],
      "released": "2021-11-30",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/fca/fca89fa281f2d144a86e8aa12d6363a8.jpg",
      "rating": 0.0,
      "rating_top": 0,
      "ratings": [],
      "ratings_count": 0,
      "reviews_text_count": 0,
      "added": 0,
      "added_by_status": {},
      "metacritic": null,
      "suggestions_count": 549,
      "updated": "2023-02-09T02:28:29",
      "id": 795314,
      "score": null,
      "clip": null,
      "tags": [],
      "esrb_rating": {
        "id": 4,
        "name": "Mature",
        "slug": "mature",
        "name_en": "Mature",
        "name_ru": "С 17 лет"
      },
      "user_game": null,
      "reviews_count": 0,
      "community_rating": 0,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/fca/fca89fa281f2d144a86e8aa12d6363a8.jpg"
        },
        {
          "id": 3400893,
          "image": "https://media.rawg.io/media/screenshots/9e4/9e4305342772178385c2b4b6fc8311bc.jpg"
        },
        {
          "id": 3400894,
          "image": "https://media.rawg.io/media/screenshots/66e/66e1eb017b377cbc9470b2e461e26e59.jpg"
        },
        {
          "id": 3400895,
          "image": "https://media.rawg.io/media/screenshots/93a/93aacf656739d6ae62827854c9ff86d3.jpg"
        },
        {
          "id": 3400896,
          "image": "https://media.rawg.io/media/screenshots/2df/2df55e92907171642680813581db47cb.jpg"
        },
        {
          "id": 3400897,
          "image": "https://media.rawg.io/media/screenshots/de7/de7ebe238098480bc774d5c974733570.jpg"
        },
        {
          "id": 3400898,
          "image": "https://media.rawg.io/media/screenshots/cf9/cf95335ef4308f648f16ee11aa479909.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "asterix-obelix-slap-them-all",
      "name": "Asterix & Obelix: Slap them All!",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 5,
            "name": "GOG",
            "slug": "gog"
          }
        }
      ],
      "released": "2021-11-25",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/a48/a48ff24c0e273c4461005acb7c454b0d.jpg",
      "rating": 2.33,
      "rating_top": 1,
      "ratings": [
        {
          "id": 1,
          "title": "skip",
          "count": 3,
          "percent": 50.0
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 2,
          "percent": 33.33
        },
        {
          "id": 3,
          "title": "meh",
          "count": 1,
          "percent": 16.67
        }
      ],
      "ratings_count": 5,
      "reviews_text_count": 1,
      "added": 20,
      "added_by_status": {
        "yet": 3,
        "owned": 3,
        "beaten": 4,
        "toplay": 8,
        "dropped": 2
      },
      "metacritic": 66,
      "suggestions_count": 212,
      "updated": "2022-12-14T11:46:18",
      "id": 635388,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 204994,
          "image_background": "https://media.rawg.io/media/games/bc0/bc06a29ceac58652b684deefe7d56099.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 33848,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 31701,
          "image_background": "https://media.rawg.io/media/games/021/021c4e21a1824d2526f925eff6324653.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30183,
          "image_background": "https://media.rawg.io/media/games/9dd/9ddabb34840ea9227556670606cf8ea3.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 34834,
          "image_background": "https://media.rawg.io/media/games/34b/34b1f1850a1c06fd971bc6ab3ac0ce0e.jpg"
        },
        {
          "id": 42425,
          "name": "Для нескольких игроков",
          "slug": "dlia-neskolkikh-igrokov",
          "language": "rus",
          "games_count": 7707,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 9732,
          "image_background": "https://media.rawg.io/media/games/b49/b4912b5dbfc7ed8927b65f05b8507f6c.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 4026,
          "image_background": "https://media.rawg.io/media/games/baf/baf9905270314e07e6850cffdb51df41.jpg"
        },
        {
          "id": 45,
          "name": "2D",
          "slug": "2d",
          "language": "eng",
          "games_count": 187536,
          "image_background": "https://media.rawg.io/media/games/f46/f466571d536f2e3ea9e815ad17177501.jpg"
        },
        {
          "id": 198,
          "name": "Split Screen",
          "slug": "split-screen",
          "language": "eng",
          "games_count": 5543,
          "image_background": "https://media.rawg.io/media/games/bd2/bd2cc7714e0b9b1adad1ba1b2400d436.jpg"
        },
        {
          "id": 42449,
          "name": "Локальный мультиплеер",
          "slug": "lokalnyi-multipleer",
          "language": "rus",
          "games_count": 2888,
          "image_background": "https://media.rawg.io/media/games/d1a/d1a1202a378607b6c635c8f18ace95dd.jpg"
        },
        {
          "id": 75,
          "name": "Local Co-Op",
          "slug": "local-co-op",
          "language": "eng",
          "games_count": 4955,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 72,
          "name": "Local Multiplayer",
          "slug": "local-multiplayer",
          "language": "eng",
          "games_count": 12456,
          "image_background": "https://media.rawg.io/media/games/fc8/fc838d98c9b944e6a15176eabf40bee8.jpg"
        },
        {
          "id": 203,
          "name": "Beat 'em up",
          "slug": "beat-em-up",
          "language": "eng",
          "games_count": 2673,
          "image_background": "https://media.rawg.io/media/games/8e6/8e699e91cf77c2060b6d515e2135b1b1.jpg"
        },
        {
          "id": 1465,
          "name": "combat",
          "slug": "combat",
          "language": "eng",
          "games_count": 9214,
          "image_background": "https://media.rawg.io/media/games/686/686909717c3aa01518bc42ae2bf4259e.jpg"
        },
        {
          "id": 42483,
          "name": "Рисованная графика",
          "slug": "risovannaia-grafika",
          "language": "rus",
          "games_count": 2742,
          "image_background": "https://media.rawg.io/media/games/1f4/1f47a270b8f241e4676b14d39ec620f7.jpg"
        },
        {
          "id": 258,
          "name": "Hand-drawn",
          "slug": "hand-drawn",
          "language": "eng",
          "games_count": 5632,
          "image_background": "https://media.rawg.io/media/games/d87/d8788f5bdc90c142f60f66f99cfdd97b.jpg"
        },
        {
          "id": 268,
          "name": "Comic Book",
          "slug": "comic-book",
          "language": "eng",
          "games_count": 851,
          "image_background": "https://media.rawg.io/media/games/a2d/a2d80d32a35281c310a2b9d8cb02e5cf.jpg"
        },
        {
          "id": 42525,
          "name": "По комиксу",
          "slug": "po-komiksu",
          "language": "rus",
          "games_count": 871,
          "image_background": "https://media.rawg.io/media/screenshots/bd8/bd89aa5d49198737aae675c00fbea355.jpg"
        },
        {
          "id": 744,
          "name": "friends",
          "slug": "friends",
          "language": "eng",
          "games_count": 15057,
          "image_background": "https://media.rawg.io/media/screenshots/b2a/b2a07062fed4f1bfb52f4ec4ab4fbb86.jpg"
        },
        {
          "id": 58127,
          "name": "Бой",
          "slug": "boi-2",
          "language": "rus",
          "games_count": 4152,
          "image_background": "https://media.rawg.io/media/games/64e/64e2a77f37ddc48d102127234af99886.jpg"
        },
        {
          "id": 135,
          "name": "2D Fighter",
          "slug": "2d-fighter",
          "language": "eng",
          "games_count": 814,
          "image_background": "https://media.rawg.io/media/games/96b/96b9420bbd4bd1b79bf2a67f7779733a.jpg"
        },
        {
          "id": 45201,
          "name": "Remote Play Together",
          "slug": "remote-play-together",
          "language": "eng",
          "games_count": 1834,
          "image_background": "https://media.rawg.io/media/games/d47/d479582ed0a46496ad34f65c7099d7e5.jpg"
        },
        {
          "id": 1529,
          "name": "fight",
          "slug": "fight",
          "language": "eng",
          "games_count": 7802,
          "image_background": "https://media.rawg.io/media/screenshots/7f2/7f280778b23561fe3ea8ffa04d5ed61f.jpg"
        },
        {
          "id": 46112,
          "name": "Shared/Split Screen Co-op",
          "slug": "sharedsplit-screen-co-op",
          "language": "eng",
          "games_count": 1194,
          "image_background": "https://media.rawg.io/media/games/d47/d479582ed0a46496ad34f65c7099d7e5.jpg"
        },
        {
          "id": 56825,
          "name": "2D-файтинг",
          "slug": "2d-faiting",
          "language": "eng",
          "games_count": 588,
          "image_background": "https://media.rawg.io/media/games/a63/a63303c51e0cbc4c99a8cf0811aab286.jpg"
        },
        {
          "id": 1212,
          "name": "combos",
          "slug": "combos",
          "language": "eng",
          "games_count": 1778,
          "image_background": "https://media.rawg.io/media/games/3ad/3adf409c2dd25ddb042a37ae9d3251e7.jpg"
        },
        {
          "id": 70354,
          "name": "Локальный кооператив",
          "slug": "lokalnyi-kooperativ",
          "language": "rus",
          "games_count": 470,
          "image_background": "https://media.rawg.io/media/screenshots/89f/89f181627525c4d8c6d03bd25bfab234_gzfTcGv.jpg"
        },
        {
          "id": 4336,
          "name": "invaders",
          "slug": "invaders",
          "language": "eng",
          "games_count": 972,
          "image_background": "https://media.rawg.io/media/screenshots/b89/b89c26714f9dd2ed7e289b0bb3a71d84.jpg"
        },
        {
          "id": 4027,
          "name": "travel",
          "slug": "travel",
          "language": "eng",
          "games_count": 746,
          "image_background": "https://media.rawg.io/media/screenshots/61d/61d7bcfff83048f279a32466f060eb42.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 6,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/a48/a48ff24c0e273c4461005acb7c454b0d.jpg"
        },
        {
          "id": 2935651,
          "image": "https://media.rawg.io/media/screenshots/f64/f6438e3aba3215a0675f817d17305434.jpg"
        },
        {
          "id": 2935652,
          "image": "https://media.rawg.io/media/screenshots/060/060a8e87396fb19421dcb4d9a5899671.jpg"
        },
        {
          "id": 2935653,
          "image": "https://media.rawg.io/media/screenshots/141/1413a4ad1f87c828953d60cd0dcf0e06.jpg"
        },
        {
          "id": 2935654,
          "image": "https://media.rawg.io/media/screenshots/a48/a48ff24c0e273c4461005acb7c454b0d.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "farming-simulator-22",
      "name": "Farming Simulator 22",
      "playtime": 8,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "macOS",
            "slug": "macos"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 3,
            "name": "PlayStation Store",
            "slug": "playstation-store"
          }
        },
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-11-22",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/dd9/dd90964966f6bf9b0bd635be432fbf8a.jpg",
      "rating": 3.4,
      "rating_top": 4,
      "ratings": [
        {
          "id": 4,
          "title": "recommended",
          "count": 10,
          "percent": 33.33
        },
        {
          "id": 5,
          "title": "exceptional",
          "count": 7,
          "percent": 23.33
        },
        {
          "id": 3,
          "title": "meh",
          "count": 7,
          "percent": 23.33
        },
        {
          "id": 1,
          "title": "skip",
          "count": 6,
          "percent": 20.0
        }
      ],
      "ratings_count": 30,
      "reviews_text_count": 0,
      "added": 416,
      "added_by_status": {
        "yet": 14,
        "owned": 353,
        "beaten": 5,
        "toplay": 16,
        "dropped": 21,
        "playing": 7
      },
      "metacritic": 75,
      "suggestions_count": 399,
      "updated": "2023-06-04T19:03:51",
      "id": 681398,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 205598,
          "image_background": "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 30466,
          "image_background": "https://media.rawg.io/media/games/198/1988a337305e008b41d7f536ce9b73f6.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 34940,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 40836,
          "name": "Full controller support",
          "slug": "full-controller-support",
          "language": "eng",
          "games_count": 14289,
          "image_background": "https://media.rawg.io/media/games/b72/b7233d5d5b1e75e86bb860ccc7aeca85.jpg"
        },
        {
          "id": 42425,
          "name": "Для нескольких игроков",
          "slug": "dlia-neskolkikh-igrokov",
          "language": "rus",
          "games_count": 7776,
          "image_background": "https://media.rawg.io/media/games/b7b/b7b8381707152afc7d91f5d95de70e39.jpg"
        },
        {
          "id": 18,
          "name": "Co-op",
          "slug": "co-op",
          "language": "eng",
          "games_count": 9792,
          "image_background": "https://media.rawg.io/media/games/c6b/c6bfece1daf8d06bc0a60632ac78e5bf.jpg"
        },
        {
          "id": 42442,
          "name": "Открытый мир",
          "slug": "otkrytyi-mir",
          "language": "rus",
          "games_count": 4426,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 36,
          "name": "Open World",
          "slug": "open-world",
          "language": "eng",
          "games_count": 6249,
          "image_background": "https://media.rawg.io/media/games/7cf/7cfc9220b401b7a300e409e539c9afd5.jpg"
        },
        {
          "id": 411,
          "name": "cooperative",
          "slug": "cooperative",
          "language": "eng",
          "games_count": 4058,
          "image_background": "https://media.rawg.io/media/games/46d/46d98e6910fbc0706e2948a7cc9b10c5.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 28468,
          "image_background": "https://media.rawg.io/media/games/6c5/6c55e22185876626881b76c11922b073.jpg"
        },
        {
          "id": 42429,
          "name": "От первого лица",
          "slug": "ot-pervogo-litsa",
          "language": "rus",
          "games_count": 7523,
          "image_background": "https://media.rawg.io/media/games/7a2/7a2500ee8b2c0e1ff268bb4479463dea.jpg"
        },
        {
          "id": 149,
          "name": "Third Person",
          "slug": "third-person",
          "language": "eng",
          "games_count": 9386,
          "image_background": "https://media.rawg.io/media/games/d82/d82990b9c67ba0d2d09d4e6fa88885a7.jpg"
        },
        {
          "id": 42441,
          "name": "От третьего лица",
          "slug": "ot-tretego-litsa",
          "language": "rus",
          "games_count": 4897,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42413,
          "name": "Симулятор",
          "slug": "simuliator",
          "language": "rus",
          "games_count": 15029,
          "image_background": "https://media.rawg.io/media/games/dd5/dd50d4266915d56dd5b63ae1bf72606a.jpg"
        },
        {
          "id": 9,
          "name": "Online Co-Op",
          "slug": "online-co-op",
          "language": "eng",
          "games_count": 4361,
          "image_background": "https://media.rawg.io/media/games/530/5302dd22a190e664531236ca724e8726.jpg"
        },
        {
          "id": 37,
          "name": "Sandbox",
          "slug": "sandbox",
          "language": "eng",
          "games_count": 5885,
          "image_background": "https://media.rawg.io/media/games/310/3106b0e012271c5ffb16497b070be739.jpg"
        },
        {
          "id": 42444,
          "name": "Песочница",
          "slug": "pesochnitsa",
          "language": "rus",
          "games_count": 3121,
          "image_background": "https://media.rawg.io/media/games/849/849414b978db37d4563ff9e4b0d3a787.jpg"
        },
        {
          "id": 40832,
          "name": "Cross-Platform Multiplayer",
          "slug": "cross-platform-multiplayer",
          "language": "eng",
          "games_count": 2235,
          "image_background": "https://media.rawg.io/media/games/48c/48cb04ca483be865e3a83119c94e6097.jpg"
        },
        {
          "id": 62,
          "name": "Moddable",
          "slug": "moddable",
          "language": "eng",
          "games_count": 767,
          "image_background": "https://media.rawg.io/media/games/806/8060a7663364ac23e15480728938d6f3.jpg"
        },
        {
          "id": 42562,
          "name": "Для всей семьи",
          "slug": "dlia-vsei-semi",
          "language": "rus",
          "games_count": 5511,
          "image_background": "https://media.rawg.io/media/screenshots/054/054bf49d9e736edfda5aa8e9015faf9b.jpeg"
        },
        {
          "id": 42438,
          "name": "Поддержка модификаций",
          "slug": "podderzhka-modifikatsii",
          "language": "rus",
          "games_count": 579,
          "image_background": "https://media.rawg.io/media/games/ec3/ec3a7db7b8ab5a71aad622fe7c62632f.jpg"
        },
        {
          "id": 42530,
          "name": "Кастомизация персонажа",
          "slug": "kastomizatsiia-personazha",
          "language": "rus",
          "games_count": 2702,
          "image_background": "https://media.rawg.io/media/games/9bf/9bfac18ff678f41a4674250fa0e04a52.jpg"
        },
        {
          "id": 107,
          "name": "Family Friendly",
          "slug": "family-friendly",
          "language": "eng",
          "games_count": 5220,
          "image_background": "https://media.rawg.io/media/games/1e5/1e5e33b88be978f451196a751424a72e.jpg"
        },
        {
          "id": 121,
          "name": "Character Customization",
          "slug": "character-customization",
          "language": "eng",
          "games_count": 3482,
          "image_background": "https://media.rawg.io/media/games/21c/21cc15d233117c6809ec86870559e105.jpg"
        },
        {
          "id": 571,
          "name": "3D",
          "slug": "3d",
          "language": "eng",
          "games_count": 77200,
          "image_background": "https://media.rawg.io/media/games/238/238e2b2b24c9838626700c69cacf1e3a.jpg"
        },
        {
          "id": 42551,
          "name": "Менеджмент",
          "slug": "menedzhment",
          "language": "rus",
          "games_count": 2580,
          "image_background": "https://media.rawg.io/media/games/11b/11b81edff7f45024e36b88e880d86585.jpg"
        },
        {
          "id": 67,
          "name": "Management",
          "slug": "management",
          "language": "eng",
          "games_count": 6495,
          "image_background": "https://media.rawg.io/media/games/3c9/3c994986d767f56e7b72a124a35d4c3c.jpg"
        },
        {
          "id": 42521,
          "name": "Экономика",
          "slug": "ekonomika",
          "language": "rus",
          "games_count": 1069,
          "image_background": "https://media.rawg.io/media/games/ce6/ce6c9b58be2ca76688a768cffcb043d1.jpg"
        },
        {
          "id": 195,
          "name": "Economy",
          "slug": "economy",
          "language": "eng",
          "games_count": 1890,
          "image_background": "https://media.rawg.io/media/screenshots/85c/85c91c5064d2cdd56a949c8008868318.jpg"
        },
        {
          "id": 78,
          "name": "America",
          "slug": "america",
          "language": "eng",
          "games_count": 459,
          "image_background": "https://media.rawg.io/media/games/bce/bce62fbc7cf74bf6a1a37340993ec148.jpg"
        },
        {
          "id": 42473,
          "name": "Immersive Sim",
          "slug": "immersive-sim-2",
          "language": "eng",
          "games_count": 1698,
          "image_background": "https://media.rawg.io/media/games/6ee/6ee5cbe30c6b6c299a3288337d40a0f3.jpg"
        },
        {
          "id": 774,
          "name": "nature",
          "slug": "nature",
          "language": "eng",
          "games_count": 4814,
          "image_background": "https://media.rawg.io/media/screenshots/1dd/1ddb8ea784f98fc8cb77c0e5ccd28105.jpeg"
        },
        {
          "id": 42456,
          "name": "Америка",
          "slug": "amerika",
          "language": "rus",
          "games_count": 429,
          "image_background": "https://media.rawg.io/media/games/b49/b49e403e116e9734e95b4c88e5b2ae74.jpg"
        },
        {
          "id": 70351,
          "name": "Сетевой кооператив",
          "slug": "setevoi-kooperativ",
          "language": "rus",
          "games_count": 880,
          "image_background": "https://media.rawg.io/media/games/4fe/4feffcec6315c5f5a96442a8444431ca.jpg"
        },
        {
          "id": 43374,
          "name": "Remote Play on TV",
          "slug": "remote-play-on-tv",
          "language": "eng",
          "games_count": 320,
          "image_background": "https://media.rawg.io/media/games/cd3/cd3c9c7d3e95cb1608fd6250f1b90b7a.jpg"
        },
        {
          "id": 42414,
          "name": "Сельское хозяйство",
          "slug": "selskoe-khoziaistvo",
          "language": "rus",
          "games_count": 423,
          "image_background": "https://media.rawg.io/media/games/24c/24c4678bfcdc3858e81c1efe5366c5a7.jpg"
        },
        {
          "id": 42644,
          "name": "Лошади",
          "slug": "loshadi",
          "language": "rus",
          "games_count": 149,
          "image_background": "https://media.rawg.io/media/games/a71/a7108a0074a600b9fc64ae7f9eb21ef1.jpg"
        },
        {
          "id": 128,
          "name": "Horses",
          "slug": "horses",
          "language": "eng",
          "games_count": 173,
          "image_background": "https://media.rawg.io/media/games/a71/a7108a0074a600b9fc64ae7f9eb21ef1.jpg"
        },
        {
          "id": 159,
          "name": "Agriculture",
          "slug": "agriculture",
          "language": "eng",
          "games_count": 447,
          "image_background": "https://media.rawg.io/media/games/276/276161d772f7f3f9a4520f6e9698f8c8.jpg"
        },
        {
          "id": 58125,
          "name": "Иммерсивный симулятор",
          "slug": "immersivnyi-simuliator",
          "language": "rus",
          "games_count": 1570,
          "image_background": "https://media.rawg.io/media/screenshots/dd5/dd56d723132ef97a252a2ce4665434dc.jpg"
        },
        {
          "id": 58124,
          "name": "Природа",
          "slug": "priroda",
          "language": "rus",
          "games_count": 1453,
          "image_background": "https://media.rawg.io/media/games/9e7/9e78f4baf60cb4eafdb73ff70b896fd2.jpg"
        },
        {
          "id": 844,
          "name": "simulator",
          "slug": "simulator",
          "language": "eng",
          "games_count": 7305,
          "image_background": "https://media.rawg.io/media/screenshots/b51/b51b996f17b74b01905a70e72401dafb.jpg"
        },
        {
          "id": 49969,
          "name": "Farming Sim",
          "slug": "farming-sim-2",
          "language": "eng",
          "games_count": 395,
          "image_background": "https://media.rawg.io/media/screenshots/1f7/1f75a86891fc0a3e7a58bb8f1c5bcab5.jpg"
        },
        {
          "id": 58272,
          "name": "Симулятор фермы",
          "slug": "simuliator-fermy",
          "language": "rus",
          "games_count": 352,
          "image_background": "https://media.rawg.io/media/games/196/196381a3afc69bd73d4374fdbf5ef774.jpg"
        },
        {
          "id": 513,
          "name": "coop",
          "slug": "coop",
          "language": "eng",
          "games_count": 470,
          "image_background": "https://media.rawg.io/media/screenshots/9d8/9d8a46349f7a3a150d45d766939e51a3.jpg"
        }
      ],
      "esrb_rating": {
        "id": 1,
        "name": "Everyone",
        "slug": "everyone",
        "name_en": "Everyone",
        "name_ru": "Для всех"
      },
      "user_game": null,
      "reviews_count": 30,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/dd9/dd90964966f6bf9b0bd635be432fbf8a.jpg"
        },
        {
          "id": 3071585,
          "image": "https://media.rawg.io/media/screenshots/38f/38fd5a7e40d0099c70bff22aabeb0daa.jpg"
        },
        {
          "id": 3071586,
          "image": "https://media.rawg.io/media/screenshots/baa/baa618afd34c53624d8767e535c027a9.jpg"
        },
        {
          "id": 3071587,
          "image": "https://media.rawg.io/media/screenshots/d44/d4460a6a65807acca6e6d041b38bc197.jpg"
        },
        {
          "id": 3071589,
          "image": "https://media.rawg.io/media/screenshots/3c8/3c8c286aa153e50f7db191c2a65b0312.jpg"
        },
        {
          "id": 3071590,
          "image": "https://media.rawg.io/media/screenshots/026/026d6cdd03f6997a437b9107fdb5995d.jpg"
        },
        {
          "id": 3071591,
          "image": "https://media.rawg.io/media/screenshots/199/1997298fb235da15374e023fbb37e86d.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 5,
            "name": "Apple Macintosh",
            "slug": "mac"
          }
        }
      ],
      "genres": [
        {
          "id": 14,
          "name": "Simulation",
          "slug": "simulation"
        }
      ]
    },
    {
      "slug": "nerf-legends",
      "name": "Nerf Legends",
      "playtime": 1,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        },
        {
          "platform": {
            "id": 1,
            "name": "Xbox One",
            "slug": "xbox-one"
          }
        },
        {
          "platform": {
            "id": 18,
            "name": "PlayStation 4",
            "slug": "playstation4"
          }
        },
        {
          "platform": {
            "id": 186,
            "name": "Xbox Series S/X",
            "slug": "xbox-series-x"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo Switch",
            "slug": "nintendo-switch"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 1,
            "name": "Steam",
            "slug": "steam"
          }
        },
        {
          "store": {
            "id": 6,
            "name": "Nintendo Store",
            "slug": "nintendo"
          }
        }
      ],
      "released": "2021-11-18",
      "tba": false,
      "background_image": "https://media.rawg.io/media/screenshots/958/958b7529437d3daf8b3a23567fd5da58.jpg",
      "rating": 0.0,
      "rating_top": 0,
      "ratings": [
        {
          "id": 3,
          "title": "meh",
          "count": 1,
          "percent": 100.0
        }
      ],
      "ratings_count": 1,
      "reviews_text_count": 0,
      "added": 16,
      "added_by_status": {
        "owned": 14,
        "beaten": 1,
        "dropped": 1
      },
      "metacritic": null,
      "suggestions_count": 248,
      "updated": "2022-11-21T15:53:31",
      "id": 684912,
      "score": null,
      "clip": null,
      "tags": [
        {
          "id": 31,
          "name": "Singleplayer",
          "slug": "singleplayer",
          "language": "eng",
          "games_count": 209582,
          "image_background": "https://media.rawg.io/media/games/73e/73eecb8909e0c39fb246f457b5d6cbbe.jpg"
        },
        {
          "id": 42396,
          "name": "Для одного игрока",
          "slug": "dlia-odnogo-igroka",
          "language": "rus",
          "games_count": 32259,
          "image_background": "https://media.rawg.io/media/games/618/618c2031a07bbff6b4f611f10b6bcdbc.jpg"
        },
        {
          "id": 42417,
          "name": "Экшен",
          "slug": "ekshen",
          "language": "rus",
          "games_count": 30948,
          "image_background": "https://media.rawg.io/media/games/d58/d588947d4286e7b5e0e12e1bea7d9844.jpg"
        },
        {
          "id": 40847,
          "name": "Steam Achievements",
          "slug": "steam-achievements",
          "language": "eng",
          "games_count": 29451,
          "image_background": "https://media.rawg.io/media/games/8cc/8cce7c0e99dcc43d66c8efd42f9d03e3.jpg"
        },
        {
          "id": 7,
          "name": "Multiplayer",
          "slug": "multiplayer",
          "language": "eng",
          "games_count": 35586,
          "image_background": "https://media.rawg.io/media/games/328/3283617cb7d75d67257fc58339188742.jpg"
        },
        {
          "id": 42425,
          "name": "Для нескольких игроков",
          "slug": "dlia-neskolkikh-igrokov",
          "language": "rus",
          "games_count": 7497,
          "image_background": "https://media.rawg.io/media/games/d69/d69810315bd7e226ea2d21f9156af629.jpg"
        },
        {
          "id": 42428,
          "name": "Шутер",
          "slug": "shuter",
          "language": "rus",
          "games_count": 6356,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 8,
          "name": "First-Person",
          "slug": "first-person",
          "language": "eng",
          "games_count": 29119,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 42429,
          "name": "От первого лица",
          "slug": "ot-pervogo-litsa",
          "language": "rus",
          "games_count": 7085,
          "image_background": "https://media.rawg.io/media/games/fc1/fc1307a2774506b5bd65d7e8424664a7.jpg"
        },
        {
          "id": 32,
          "name": "Sci-fi",
          "slug": "sci-fi",
          "language": "eng",
          "games_count": 17348,
          "image_background": "https://media.rawg.io/media/games/8e4/8e4de3f54ac659e08a7ba6a2b731682a.jpg"
        },
        {
          "id": 42423,
          "name": "Научная фантастика",
          "slug": "nauchnaia-fantastika",
          "language": "rus",
          "games_count": 5689,
          "image_background": "https://media.rawg.io/media/games/951/951572a3dd1e42544bd39a5d5b42d234.jpg"
        },
        {
          "id": 40845,
          "name": "Partial Controller Support",
          "slug": "partial-controller-support",
          "language": "eng",
          "games_count": 9487,
          "image_background": "https://media.rawg.io/media/games/7fa/7fa0b586293c5861ee32490e953a4996.jpg"
        },
        {
          "id": 30,
          "name": "FPS",
          "slug": "fps",
          "language": "eng",
          "games_count": 12564,
          "image_background": "https://media.rawg.io/media/games/120/1201a40e4364557b124392ee50317b99.jpg"
        },
        {
          "id": 42427,
          "name": "Шутер от первого лица",
          "slug": "shuter-ot-pervogo-litsa",
          "language": "rus",
          "games_count": 3883,
          "image_background": "https://media.rawg.io/media/games/c4b/c4b0cab189e73432de3a250d8cf1c84e.jpg"
        },
        {
          "id": 42480,
          "name": "Фэнтези",
          "slug": "fentezi",
          "language": "rus",
          "games_count": 7618,
          "image_background": "https://media.rawg.io/media/screenshots/88b/88b5f27f07d6ca2f8a3cd0b36e03a670.jpg"
        },
        {
          "id": 64,
          "name": "Fantasy",
          "slug": "fantasy",
          "language": "eng",
          "games_count": 24635,
          "image_background": "https://media.rawg.io/media/screenshots/88b/88b5f27f07d6ca2f8a3cd0b36e03a670.jpg"
        },
        {
          "id": 42416,
          "name": "Контроллер",
          "slug": "kontroller",
          "language": "rus",
          "games_count": 3517,
          "image_background": "https://media.rawg.io/media/games/0b3/0b34647c42271600399b93d19b10f1aa.jpg"
        },
        {
          "id": 115,
          "name": "Controller",
          "slug": "controller",
          "language": "eng",
          "games_count": 9264,
          "image_background": "https://media.rawg.io/media/games/4cb/4cb855e8ef1578415a928e53c9f51867.png"
        },
        {
          "id": 157,
          "name": "PvP",
          "slug": "pvp",
          "language": "eng",
          "games_count": 6965,
          "image_background": "https://media.rawg.io/media/games/6fc/6fcf4cd3b17c288821388e6085bb0fc9.jpg"
        },
        {
          "id": 42434,
          "name": "Игрок против игрока",
          "slug": "igrok-protiv-igroka",
          "language": "rus",
          "games_count": 3309,
          "image_background": "https://media.rawg.io/media/games/26b/26b27e1da9e3727fcb12e3e4e86c8c19.jpg"
        },
        {
          "id": 42530,
          "name": "Кастомизация персонажа",
          "slug": "kastomizatsiia-personazha",
          "language": "rus",
          "games_count": 2519,
          "image_background": "https://media.rawg.io/media/games/f54/f54e9fb2f4aac37810ea1a69a3e4480a.jpg"
        },
        {
          "id": 121,
          "name": "Character Customization",
          "slug": "character-customization",
          "language": "eng",
          "games_count": 3341,
          "image_background": "https://media.rawg.io/media/games/21c/21cc15d233117c6809ec86870559e105.jpg"
        },
        {
          "id": 167,
          "name": "Futuristic",
          "slug": "futuristic",
          "language": "eng",
          "games_count": 4223,
          "image_background": "https://media.rawg.io/media/games/ebd/ebdbb7eb52bd58b0e7fa4538d9757b60.jpg"
        },
        {
          "id": 42451,
          "name": "Будущее",
          "slug": "budushchee",
          "language": "rus",
          "games_count": 2281,
          "image_background": "https://media.rawg.io/media/screenshots/0e6/0e60075818860647a1e6a9f9a8ebfada.jpg"
        },
        {
          "id": 1465,
          "name": "combat",
          "slug": "combat",
          "language": "eng",
          "games_count": 8941,
          "image_background": "https://media.rawg.io/media/games/048/048b46cdc66cbc7e235e1f359c2a77ec.jpg"
        },
        {
          "id": 45878,
          "name": "Online PvP",
          "slug": "online-pvp",
          "language": "eng",
          "games_count": 2955,
          "image_background": "https://media.rawg.io/media/games/511/5118aff5091cb3efec399c808f8c598f.jpg"
        },
        {
          "id": 197,
          "name": "Robots",
          "slug": "robots",
          "language": "eng",
          "games_count": 7901,
          "image_background": "https://media.rawg.io/media/games/26d/26d4437715bee60138dab4a7c8c59c92.jpg"
        },
        {
          "id": 42630,
          "name": "Роботы",
          "slug": "roboty",
          "language": "rus",
          "games_count": 1274,
          "image_background": "https://media.rawg.io/media/games/396/3963e0df75c22d5995368ec43dacc19e.jpg"
        },
        {
          "id": 58127,
          "name": "Бой",
          "slug": "boi-2",
          "language": "rus",
          "games_count": 3838,
          "image_background": "https://media.rawg.io/media/screenshots/338/338a51ae1446dd070a8ae9a3ccf74c51.jpg"
        },
        {
          "id": 1500,
          "name": "immersive",
          "slug": "immersive",
          "language": "eng",
          "games_count": 778,
          "image_background": "https://media.rawg.io/media/screenshots/816/816377afeda192150b8397f738c33619.jpg"
        },
        {
          "id": 58140,
          "name": "Иммерсивная игра",
          "slug": "immersivnaia-igra",
          "language": "rus",
          "games_count": 95,
          "image_background": "https://media.rawg.io/media/games/400/4002e3aa52cf33d184f0f74cc2348134.jpg"
        }
      ],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 1,
      "community_rating": 0,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/screenshots/958/958b7529437d3daf8b3a23567fd5da58.jpg"
        },
        {
          "id": 3081936,
          "image": "https://media.rawg.io/media/screenshots/c49/c493c3ab851de0cc4777e9079d0b74a2.jpg"
        },
        {
          "id": 3081937,
          "image": "https://media.rawg.io/media/screenshots/8af/8af7bc91a7f4ee8726282b44d3dd32a8.jpg"
        },
        {
          "id": 3081938,
          "image": "https://media.rawg.io/media/screenshots/728/72855b340a3b0c0e0fb614996a9cbce0.jpg"
        },
        {
          "id": 3081939,
          "image": "https://media.rawg.io/media/screenshots/958/958b7529437d3daf8b3a23567fd5da58.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        },
        {
          "platform": {
            "id": 3,
            "name": "Xbox",
            "slug": "xbox"
          }
        },
        {
          "platform": {
            "id": 7,
            "name": "Nintendo",
            "slug": "nintendo"
          }
        }
      ],
      "genres": [
        {
          "id": 4,
          "name": "Action",
          "slug": "action"
        }
      ]
    },
    {
      "slug": "kid-a-mnesia-exhibition",
      "name": "Kid A Mnesia: Exhibition",
      "playtime": 0,
      "platforms": [
        {
          "platform": {
            "id": 4,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 187,
            "name": "PlayStation 5",
            "slug": "playstation5"
          }
        }
      ],
      "stores": [
        {
          "store": {
            "id": 11,
            "name": "Epic Games",
            "slug": "epic-games"
          }
        }
      ],
      "released": "2021-11-18",
      "tba": false,
      "background_image": "https://media.rawg.io/media/games/ccc/ccc677ad4cf5e29aee30093c5d228e5f.jpg",
      "rating": 4.16,
      "rating_top": 5,
      "ratings": [
        {
          "id": 5,
          "title": "exceptional",
          "count": 13,
          "percent": 41.94
        },
        {
          "id": 4,
          "title": "recommended",
          "count": 12,
          "percent": 38.71
        },
        {
          "id": 3,
          "title": "meh",
          "count": 5,
          "percent": 16.13
        },
        {
          "id": 1,
          "title": "skip",
          "count": 1,
          "percent": 3.23
        }
      ],
      "ratings_count": 31,
      "reviews_text_count": 0,
      "added": 113,
      "added_by_status": {
        "yet": 44,
        "owned": 26,
        "beaten": 29,
        "toplay": 6,
        "dropped": 8
      },
      "metacritic": null,
      "suggestions_count": 0,
      "updated": "2023-04-29T13:25:35",
      "id": 692545,
      "score": null,
      "clip": null,
      "tags": [],
      "esrb_rating": null,
      "user_game": null,
      "reviews_count": 31,
      "saturated_color": "0f0f0f",
      "dominant_color": "0f0f0f",
      "short_screenshots": [
        {
          "id": -1,
          "image": "https://media.rawg.io/media/games/ccc/ccc677ad4cf5e29aee30093c5d228e5f.jpg"
        }
      ],
      "parent_platforms": [
        {
          "platform": {
            "id": 1,
            "name": "PC",
            "slug": "pc"
          }
        },
        {
          "platform": {
            "id": 2,
            "name": "PlayStation",
            "slug": "playstation"
          }
        }
      ],
      "genres": [
        {
          "id": 3,
          "name": "Adventure",
          "slug": "adventure"
        }
      ]
    }
  ],
  "user_platforms": false
}

''';
