import 'package:flutter_coding_challenge/utils/resources/env.dart';

import 'init_app.dart';

void main() async {
  initApp(EnvType.development);
}
