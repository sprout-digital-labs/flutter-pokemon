import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_coding_challenge/presentation/events/pokemon_list_event.dart';
import 'package:flutter_coding_challenge/presentation/states/pokemon_list_state.dart';
import 'package:injectable/injectable.dart';

import '../../config/router/navigation_config.dart';
import '../../domain/models/response/pokemon_list_response.dart';
import '../../domain/repositories/api_repository.dart';

var pokemonListBloc = navigationService.navigatorKey.currentContext?.read<PokemonListBloc>();

@injectable
class PokemonListBloc extends Bloc<PokemonListEvent, PokemonListState> {
  final ApiRepository apiRepository;

  ListLayout listLayout = ListLayout.listview;
  List<PokemonListData> result = [];
  int page = 0;
  bool isLoading = false;

  PokemonListBloc(this.apiRepository) : super(PokemonListStateInit()) {
    on<PokemonListFetchEvent>((event, emit) async {
      if (!isLoading) {
        isLoading = true;

        if (event.isFirstPage) {
          emit(PokemonListStateLoading());
          result.clear();
        } else {
          emit(PokemonListStateLoadMore());
        }

        try {
          var response = await apiRepository.fetchPokemonList(page);

          result.addAll(response.results ?? []);

          emit(PokemonListStateSuccess(result: result, listLayout: listLayout));

          page += 1;
        } catch (e) {
          emit(PokemonListStateError(error: e.toString()));
        }

        isLoading = false;
      }
    });

    on<PokemonListChangeLayoutEvent>((event, emit) async {
      emit(PokemonListStateLoading());

      if (listLayout == ListLayout.listview) {
        listLayout = ListLayout.gridview;
      } else {
        listLayout = ListLayout.listview;
      }

      emit(PokemonListStateSuccess(result: result, listLayout: listLayout));

      emit(PokemonListStateLayoutType(layoutType: listLayout));
    });
  }
}
