import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../../config/router/navigation_config.dart';
import '../../domain/repositories/api_repository.dart';
import '../events/pokemon_detail_event.dart';
import '../states/pokemon_detail_state.dart';

var pokemonDetailBloc = navigationService.navigatorKey.currentContext?.read<PokemonDetailBloc>();

@injectable
class PokemonDetailBloc extends Bloc<PokemonDetailEvent, PokemonDetailState> {
  final ApiRepository apiRepository;

  PokemonDetailBloc(this.apiRepository) : super(PokemonDetailInitState()) {
    on<PokemonDetailEventFetch>((event, emit) async {
      emit(PokemonDetailLoadingState());

      try {
        var response = await apiRepository.fetchPokemonDetail(event.id);

        emit(PokemonDetailSuccessState(response));
      } catch (e) {
        emit(PokemonDetailErrorState(e.toString()));
      }
    });
    on<PokemonDetailTabEvent>((event, emit) async {
      emit(PokemonDetailLoadTabState());

      emit(PokemonDetailTabState(event.pos));
    });
  }
}
