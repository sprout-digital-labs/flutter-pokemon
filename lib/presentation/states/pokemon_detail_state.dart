import 'package:equatable/equatable.dart';

import '../../domain/models/response/pokemon_detail_response.dart';

abstract class PokemonDetailState extends Equatable {
  @override
  List<Object?> get props => [];
}

class PokemonDetailInitState extends PokemonDetailState {}

class PokemonDetailLoadingState extends PokemonDetailState {}

class PokemonDetailLoadTabState extends PokemonDetailState {}

class PokemonDetailErrorState extends PokemonDetailState {
  final String error;

  PokemonDetailErrorState(this.error);
}

class PokemonDetailSuccessState extends PokemonDetailState {
  final PokemonDetailResponse result;

  PokemonDetailSuccessState(this.result);
}

class PokemonDetailTabState extends PokemonDetailState {
  final int position;

  PokemonDetailTabState(this.position);
}
