import 'package:equatable/equatable.dart';

import '../../domain/models/response/pokemon_list_response.dart';
import '../events/pokemon_list_event.dart';

abstract class PokemonListState extends Equatable {
  @override
  List<Object?> get props => [];
}

class PokemonListStateInit extends PokemonListState {}

class PokemonListStateLoading extends PokemonListState {}

class PokemonListStateLoadMore extends PokemonListState {}

class PokemonListStateError extends PokemonListState {
  final String error;

  PokemonListStateError({required this.error});
}

class PokemonListStateSuccess extends PokemonListState {
  final List<PokemonListData> result;
  final ListLayout listLayout;

  PokemonListStateSuccess({required this.result, required this.listLayout});
}

class PokemonListStateLayoutType extends PokemonListState {
  final ListLayout layoutType;

  PokemonListStateLayoutType({required this.layoutType});
}
