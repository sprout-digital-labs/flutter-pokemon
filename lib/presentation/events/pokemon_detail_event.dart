import 'package:equatable/equatable.dart';

abstract class PokemonDetailEvent extends Equatable {
  const PokemonDetailEvent();

  @override
  List<Object?> get props => [];
}

class PokemonDetailEventFetch extends PokemonDetailEvent {
  final int id;

  const PokemonDetailEventFetch(this.id);
}

class PokemonDetailTabEvent extends PokemonDetailEvent {
  final int pos;

  const PokemonDetailTabEvent(this.pos);
}
