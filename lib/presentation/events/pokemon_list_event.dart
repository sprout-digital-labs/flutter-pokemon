import 'package:equatable/equatable.dart';

abstract class PokemonListEvent extends Equatable {
  const PokemonListEvent();

  @override
  List<Object?> get props => [];
}

class PokemonListFetchEvent extends PokemonListEvent {
  final bool isFirstPage;
  const PokemonListFetchEvent(this.isFirstPage);
}

enum ListLayout { listview, gridview }

class PokemonListChangeLayoutEvent extends PokemonListEvent {
  const PokemonListChangeLayoutEvent();
}
