import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_coding_challenge/presentation/blocs/pokemon_list_bloc.dart';
import 'package:flutter_coding_challenge/presentation/states/locale_state.dart';
import 'package:flutter_coding_challenge/presentation/views/pokemon_list_view_ext.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../generated/l10n.dart';
import '../../utils/constants/image_path.dart';
import '../blocs/locale_bloc.dart';
import '../events/locale_event.dart';
import '../events/pokemon_list_event.dart';
import '../states/pokemon_list_state.dart';

class PokemonListView extends StatefulWidget {
  const PokemonListView({Key? key}) : super(key: key);

  @override
  State<PokemonListView> createState() => _PokemonListViewState();
}

class _PokemonListViewState extends State<PokemonListView> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 1), () {
      pokemonListBloc?.add(const PokemonListFetchEvent(true));
    });

    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent == scrollController.position.pixels) {
        pokemonListBloc?.add(const PokemonListFetchEvent(false));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(S.of(context).main_title),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Container(
                padding: REdgeInsets.all(10),
                child: InkWell(
                  key: const Key("changeLanguageButton"),
                  onTap: () {
                    localeBloc?.add(const LocaleChangeLangEvent());
                  },
                  child: BlocBuilder<LocaleBloc, LocaleState>(
                    buildWhen: (prev, state) => state is LocaleStateLoading || state is LocaleStateChangeLocale,
                    builder: (context, state) {
                      if (state is LocaleStateChangeLocale) {
                        if (langModel?.currLang.countryCode == "en") {
                          return Image.asset(
                            enIc,
                            width: 20.w,
                            height: 20.h,
                          );
                        }
                      }

                      return Image.asset(
                        idIc,
                        width: 20.w,
                        height: 20.h,
                      );
                    },
                  ),
                ),
              ),
              const Spacer(),
              Container(
                padding: REdgeInsets.all(10),
                child: InkWell(
                  key: const Key("GameListChangeLayoutEvent"),
                  onTap: () {
                    pokemonListBloc?.add(const PokemonListChangeLayoutEvent());
                  },
                  child: BlocBuilder<PokemonListBloc, PokemonListState>(
                    buildWhen: (prev, state) => state is PokemonListStateLayoutType,
                    builder: (context, state) {
                      if (state is PokemonListStateLayoutType) {
                        return Text(state.layoutType.name.toLowerCase());
                      }

                      return const Text("Listview");
                    },
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: BlocBuilder<PokemonListBloc, PokemonListState>(
              buildWhen: (previousState, state) {
                return state is PokemonListStateLoading ||
                    state is PokemonListStateSuccess ||
                    state is PokemonListStateError;
              },
              builder: (context, state) {
                if (state is PokemonListStateLoading) {
                  return const Center(
                    child: RefreshProgressIndicator(),
                  );
                } else if (state is PokemonListStateSuccess) {
                  return state.listLayout == ListLayout.listview
                      ? listviewLayout(scrollController, state.result)
                      : gridviewLayout(scrollController, state.result);
                } else if (state is PokemonListStateError) {
                  return Center(child: Text(state.error));
                }

                return Container();
              },
            ),
          ),
          BlocBuilder<PokemonListBloc, PokemonListState>(
            builder: (context, state) {
              if (state is PokemonListStateLoadMore) {
                return const Center(
                  child: RefreshProgressIndicator(),
                );
              }

              return Container();
            },
          )
        ],
      ),
    );
  }
}
