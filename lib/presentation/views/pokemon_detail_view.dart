import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_coding_challenge/presentation/views/pokemon_detail_view_ext.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../domain/models/args/pokemon_detail_args.dart';
import '../../generated/l10n.dart';
import '../blocs/pokemon_detail_bloc.dart';
import '../events/pokemon_detail_event.dart';
import '../states/pokemon_detail_state.dart';
import '../widgets/cached_network_image_utils.dart';

class PokemonDetailView extends StatefulWidget {
  const PokemonDetailView({Key? key, this.args}) : super(key: key);

  final PokemonDetailArgs? args;

  @override
  State<PokemonDetailView> createState() => _PokemonDetailViewState();
}

class _PokemonDetailViewState extends State<PokemonDetailView> with TickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () => pokemonDetailBloc?.add(PokemonDetailEventFetch(widget.args?.id ?? 0)));

    tabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocBuilder<PokemonDetailBloc, PokemonDetailState>(
            buildWhen: (prevState, state) =>
                state is PokemonDetailSuccessState ||
                state is PokemonDetailLoadingState ||
                state is PokemonDetailSuccessState,
            builder: (context, state) {
              if (state is PokemonDetailLoadingState) {
                return const Center(
                  child: RefreshProgressIndicator(),
                );
              } else if (state is PokemonDetailSuccessState) {
                var detail = state.result;

                return NestedScrollView(
                  headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        expandedHeight: 200.0,
                        pinned: true,
                        // backgroundColor: widget.args?.bagColor,
                        flexibleSpace: FlexibleSpaceBar(
                            background: Stack(
                          children: [
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(25),
                                      topRight: Radius.circular(25),
                                    ),
                                    color: Colors.white),
                                width: double.infinity,
                                height: 30,
                              ),
                            ),
                            Container(
                              padding: REdgeInsets.symmetric(horizontal: 30),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              widget.args?.title ?? '',
                                              style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            SizedBox(
                                              height: 30,
                                              child: ListView.separated(
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.horizontal,
                                                  physics: const NeverScrollableScrollPhysics(),
                                                  itemBuilder: (context, index) {
                                                    return Center(
                                                      child: Container(
                                                        // margin: REdgeInsets.all(10),
                                                        padding: REdgeInsets.all(5),
                                                        decoration: BoxDecoration(
                                                            border: Border.all(color: Colors.greenAccent),
                                                            borderRadius: const BorderRadius.all(Radius.circular(10)),
                                                            color: Colors.greenAccent),
                                                        child: Text(
                                                          "${detail.types?[index].type?.name}",
                                                          style: const TextStyle(color: Colors.white, fontSize: 14),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                  separatorBuilder: (context, index) => const SizedBox(
                                                        width: 5,
                                                      ),
                                                  itemCount: detail.types?.length ?? 0),
                                            )
                                          ],
                                        ),
                                        Text("#${widget.args?.id.toString().padLeft(3, '0')}",
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ))
                                      ],
                                    ),
                                    AppCachedNetworkImage(
                                      url: "${widget.args?.id}",
                                      height: 100,
                                      width: 100,
                                      fit: BoxFit.fitHeight,
                                      isSprites: true,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )),
                      ),
                      SliverPersistentHeader(
                        delegate: _SliverAppBarDelegate(
                          TabBar(
                            controller: tabController,
                            labelColor: Colors.black87,
                            unselectedLabelColor: Colors.grey,
                            tabs: const [
                              Tab(text: "About"),
                              Tab(text: "Base stats"),
                              Tab(text: "Sprites"),
                              Tab(text: "Moves"),
                            ],
                            onTap: (pos) {
                              pokemonDetailBloc?.add(PokemonDetailTabEvent(pos));
                            },
                          ),
                        ),
                        pinned: true,
                      ),
                    ];
                  },
                  body: BlocBuilder<PokemonDetailBloc, PokemonDetailState>(
                    buildWhen: (prevState, state) =>
                        state is PokemonDetailTabState || state is PokemonDetailLoadTabState,
                    builder: (context, state) {
                      int pos = 0;
                      if (state is PokemonDetailTabState) {
                        pos = state.position;
                      }

                      switch (pos) {
                        case 0:
                          return infoTab(detail);
                        case 1:
                          return baseStateTab(detail);
                        case 2:
                          return spritesTab(detail);
                        default:
                          return movesTab(detail);
                      }
                    },
                  ),
                );
              } else if (state is PokemonDetailErrorState) {
                return Text(S.of(context).game_detail_unavailable);
              }

              return Container();
            }),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox(
      height: 100.h,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
