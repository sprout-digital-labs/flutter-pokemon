import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../domain/models/response/pokemon_detail_response.dart';
import '../widgets/cached_network_image_utils.dart';

Widget infoTab(PokemonDetailResponse detail) {
  var abilities = '';

  detail.abilities?.forEach((element) {
    abilities += "${element.ability?.name}, ";
  });

  return rowField({
    "Species": detail.species?.name ?? "",
    "Height": detail.height.toString(),
    "Weight": detail.weight.toString(),
    "Abilities": abilities,
  });
}

Widget baseStateTab(PokemonDetailResponse detail) {
  return baseStatsRowField(detail.stats ?? []);
}

Widget spritesTab(PokemonDetailResponse detail) {
  List<String> urls = [];

  if (detail.sprites?.backDefault != null) {
    urls.add(detail.sprites?.backDefault ?? "");
  }
  if (detail.sprites?.backFemale != null) {
    urls.add(detail.sprites?.backFemale ?? "");
  }
  if (detail.sprites?.backShiny != null) {
    urls.add(detail.sprites?.backShiny ?? "");
  }
  if (detail.sprites?.backShinyFemale != null) {
    urls.add(detail.sprites?.backShinyFemale ?? "");
  }
  if (detail.sprites?.frontDefault != null) {
    urls.add(detail.sprites?.frontDefault ?? "");
  }
  if (detail.sprites?.frontFemale != null) {
    urls.add(detail.sprites?.frontFemale ?? "");
  }
  if (detail.sprites?.frontShiny != null) {
    urls.add(detail.sprites?.frontShiny ?? "");
  }
  if (detail.sprites?.frontShinyFemale != null) {
    urls.add(detail.sprites?.frontShinyFemale ?? "");
  }

  return SingleChildScrollView(
    physics: const BouncingScrollPhysics(),
    child: Column(
      children: [
        SizedBox(
          height: 200.h,
          child: ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, pos) {
                return AppCachedNetworkImage(
                  height: 100.h,
                  width: 200.w,
                  url: urls[pos],
                );
              },
              separatorBuilder: (context, index) => Container(
                    width: 10.w,
                  ),
              itemCount: urls.length),
        )
      ],
    ),
  );
}

Widget movesTab(PokemonDetailResponse detail) {
  var moves = '';

  detail.moves?.forEach((element) {
    moves += "${element.move?.name}, ";
  });

  return rowField({"Moves": moves});
}

Widget rowField(Map<String, String> child) {
  return ListView.separated(
    shrinkWrap: true,
    physics: const NeverScrollableScrollPhysics(),
    itemBuilder: (BuildContext context, int index) {
      var val = child.values.toList()[index];
      var key = child.keys.toList()[index];
      return Row(children: [
        Flexible(
          fit: FlexFit.tight,
          flex: 25,
          child: Text(key,
              style: const TextStyle(
                fontSize: 16,
              )),
        ),
        Flexible(
          fit: FlexFit.tight,
          flex: 75,
          child: Text(val,
              style: const TextStyle(
                fontSize: 14,
              )),
        )
      ]);
    },
    separatorBuilder: (BuildContext context, int index) => const SizedBox(
      height: 5,
    ),
    itemCount: child.length,
  );
}

Widget baseStatsRowField(List<BaseStatsResponse> stats) {
  var statsTots = 0;
  for (var e in stats) {
    statsTots += e.baseStat ?? 0;
  }

  var avg = statsTots / stats.length;

  return ListView.separated(
    padding: REdgeInsets.symmetric(horizontal: 10),
    shrinkWrap: true,
    physics: const NeverScrollableScrollPhysics(),
    itemBuilder: (BuildContext context, int index) {
      var val = stats[index];
      var base = 100 - (val.baseStat ?? 0);
      var opBase = 100 - base;
      return Row(children: [
        Flexible(
          fit: FlexFit.tight,
          flex: 25,
          child: Text("${val.stat?.name}",
              style: const TextStyle(
                fontSize: 20,
              )),
        ),
        Flexible(
          fit: FlexFit.tight,
          flex: 10,
          child: Text("${val.baseStat}",
              style: const TextStyle(
                fontSize: 20,
              )),
        ),
        Container(
          child: Flexible(
            fit: FlexFit.tight,
            flex: 65,
            child: Row(
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  flex: opBase,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                      color: (val.baseStat ?? 0) < avg ? Colors.red : Colors.green,
                    ),
                    height: 10,
                  ),
                ),
                Flexible(
                  fit: FlexFit.tight,
                  flex: base,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.only(bottomRight: Radius.circular(10), topRight: Radius.circular(10)),
                      color: Colors.grey.withOpacity(0.5),
                    ),
                    height: 10,
                  ),
                )
              ],
            ),
          ),
        ),
      ]);
    },
    separatorBuilder: (BuildContext context, int index) => const SizedBox(
      height: 5,
    ),
    itemCount: stats.length,
  );
}
