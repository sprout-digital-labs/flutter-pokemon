import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_coding_challenge/utils/extensions/routes_ext.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../domain/models/args/pokemon_detail_args.dart';
import '../../domain/models/response/pokemon_list_response.dart';
import '../widgets/cached_network_image_utils.dart';

Widget listviewLayout(ScrollController scrollController, List<PokemonListData> result) {
  return ListView.separated(
      controller: scrollController,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        var item = result[index];
        var color = getBagColor();
        return InkWell(
          onTap: () => context.goToDetail(PokemonDetailArgs(item.name ?? '', index + 1, color)),
          child: Container(
            margin: REdgeInsets.all(10),
            padding: REdgeInsets.only(top: 10),
            decoration: BoxDecoration(
                border: Border.all(color: color),
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                color: color),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AppCachedNetworkImage(
                  height: 75.h,
                  width: 150.w,
                  url: "${index + 1}",
                  isSprites: true,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text("Name: ${item.name}"),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        );
      },
      separatorBuilder: (context, index) => const Divider(
            height: 2,
            color: Colors.transparent,
          ),
      itemCount: result.length);
}

Widget gridviewLayout(ScrollController scrollController, List<PokemonListData> result) {
  return GridView.builder(
    controller: scrollController,
    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 0.75),
    itemBuilder: (context, index) {
      var item = result[index];
      var color = getBagColor();
      return InkWell(
        onTap: () => context.goToDetail(PokemonDetailArgs(item.name ?? '', index + 1, color)),
        child: Container(
          margin: REdgeInsets.all(10),
          padding: REdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              border: Border.all(color: color), borderRadius: const BorderRadius.all(Radius.circular(20)), color: color),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AppCachedNetworkImage(
                height: 50.h,
                width: 100.w,
                url: "${index + 1}",
                isSprites: true,
              ),
              const SizedBox(
                height: 10,
              ),
              Text("Name: ${item.name}"),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      );
    },
    shrinkWrap: true,
    itemCount: result.length,
  );
}

Color getBagColor() {
  Random random = Random();
  int randomNumber = random.nextInt(3);

  if (randomNumber == 0) {
    return Colors.blue;
  } else if (randomNumber == 1) {
    return Colors.red;
  } else {
    return Colors.green;
  }
}
