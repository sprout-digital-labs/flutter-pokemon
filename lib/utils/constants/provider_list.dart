import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_coding_challenge/presentation/blocs/locale_bloc.dart';
import 'package:flutter_coding_challenge/presentation/states/locale_state.dart';

import '../../config/DI/di_locator.dart';
import '../../presentation/blocs/pokemon_detail_bloc.dart';
import '../../presentation/blocs/pokemon_list_bloc.dart';

var providerList = [
  BlocProvider<PokemonListBloc>(create: (_) => PokemonListBloc(locator())),
  BlocProvider<PokemonDetailBloc>(create: (_) => PokemonDetailBloc(locator())),
  BlocProvider<LocaleBloc>(create: (_) => LocaleBloc(LocaleStateInit())),
];
