import 'package:flutter/material.dart';

import '../../domain/models/args/pokemon_detail_args.dart';
import '../../presentation/views/pokemon_detail_view.dart';
import '../../presentation/views/pokemon_list_view.dart';

const String pokemonListRoutes = '/PokemonList';
const String pokemonDetailRoutes = '/PokemonDetail';

Route<dynamic> initRouter(RouteSettings settings) {
  switch (settings.name) {
    case pokemonListRoutes:
      return MaterialPageRoute(
          builder: (context) => const PokemonListView(), settings: const RouteSettings());
    default:
      return MaterialPageRoute(
          builder: (context) => PokemonDetailView(
                args: settings.arguments as PokemonDetailArgs,
              ),
          settings: const RouteSettings());
  }
}
