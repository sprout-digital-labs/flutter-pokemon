import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pokemon_list_response.g.dart';

@JsonSerializable()
class PokemonListResponse extends Equatable {
  const PokemonListResponse({this.count, this.next, this.previous, this.results});

  final int? count;
  final String? next;
  final String? previous;
  final List<PokemonListData>? results;

  factory PokemonListResponse.fromJson(Map<String, dynamic> json) {
    return _$PokemonListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PokemonListResponseToJson(this);

  @override
  List<Object?> get props => [count, next, previous, results];
}

@JsonSerializable()
class PokemonListData extends Equatable {
  const PokemonListData({
    required this.name,
    required this.url,
  });

  final String? name;
  final String? url;

  factory PokemonListData.fromJson(Map<String, dynamic> json) {
    return _$PokemonListDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PokemonListDataToJson(this);

  @override
  List<Object?> get props => [name, url];
}
