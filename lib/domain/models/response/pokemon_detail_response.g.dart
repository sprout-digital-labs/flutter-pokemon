// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PokemonDetailResponse _$PokemonDetailResponseFromJson(
        Map<String, dynamic> json) =>
    PokemonDetailResponse(
      (json['game_indices'] as List<dynamic>?)
          ?.map((e) => GameIndicesResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['moves'] as List<dynamic>?)
          ?.map((e) => MovesResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      (json['types'] as List<dynamic>?)
          ?.map((e) => TypesResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['species'] == null
          ? null
          : SpeciesResponse.fromJson(json['species'] as Map<String, dynamic>),
      (json['stats'] as List<dynamic>?)
          ?.map((e) => BaseStatsResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['weight'] as int?,
      json['height'] as int?,
      abilities: (json['abilities'] as List<dynamic>?)
          ?.map((e) => AbilitiesResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
      sprites: json['sprites'] == null
          ? null
          : SpritesResponse.fromJson(json['sprites'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PokemonDetailResponseToJson(
        PokemonDetailResponse instance) =>
    <String, dynamic>{
      'abilities': instance.abilities,
      'sprites': instance.sprites,
      'game_indices': instance.gameIndices,
      'moves': instance.moves,
      'types': instance.types,
      'stats': instance.stats,
      'species': instance.species,
      'weight': instance.weight,
      'height': instance.height,
    };

BaseStatsResponse _$BaseStatsResponseFromJson(Map<String, dynamic> json) =>
    BaseStatsResponse(
      json['base_stat'] as int?,
      json['stat'] == null
          ? null
          : BaseStatsData.fromJson(json['stat'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BaseStatsResponseToJson(BaseStatsResponse instance) =>
    <String, dynamic>{
      'base_stat': instance.baseStat,
      'stat': instance.stat,
    };

BaseStatsData _$BaseStatsDataFromJson(Map<String, dynamic> json) =>
    BaseStatsData(
      json['name'] as String?,
    );

Map<String, dynamic> _$BaseStatsDataToJson(BaseStatsData instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

SpeciesResponse _$SpeciesResponseFromJson(Map<String, dynamic> json) =>
    SpeciesResponse(
      json['name'] as String?,
    );

Map<String, dynamic> _$SpeciesResponseToJson(SpeciesResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

AbilitiesResponse _$AbilitiesResponseFromJson(Map<String, dynamic> json) =>
    AbilitiesResponse(
      json['slot'] as int?,
      json['ability'] == null
          ? null
          : AbilityData.fromJson(json['ability'] as Map<String, dynamic>),
      json['is_hidden'] as bool?,
    );

Map<String, dynamic> _$AbilitiesResponseToJson(AbilitiesResponse instance) =>
    <String, dynamic>{
      'slot': instance.slot,
      'is_hidden': instance.isHidden,
      'ability': instance.ability,
    };

AbilityData _$AbilityDataFromJson(Map<String, dynamic> json) => AbilityData(
      json['name'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$AbilityDataToJson(AbilityData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };

SpritesResponse _$SpritesResponseFromJson(Map<String, dynamic> json) =>
    SpritesResponse(
      json['front_shiny_female'] as String?,
      json['back_default'] as String?,
      json['back_female'] as String?,
      json['back_shiny'] as String?,
      json['back_shiny_female'] as String?,
      json['front_default'] as String?,
      json['front_female'] as String?,
      json['front_shiny'] as String?,
    );

Map<String, dynamic> _$SpritesResponseToJson(SpritesResponse instance) =>
    <String, dynamic>{
      'back_default': instance.backDefault,
      'back_female': instance.backFemale,
      'back_shiny': instance.backShiny,
      'back_shiny_female': instance.backShinyFemale,
      'front_default': instance.frontDefault,
      'front_female': instance.frontFemale,
      'front_shiny': instance.frontShiny,
      'front_shiny_female': instance.frontShinyFemale,
    };

MovesResponse _$MovesResponseFromJson(Map<String, dynamic> json) =>
    MovesResponse(
      json['move'] == null
          ? null
          : MovesData.fromJson(json['move'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MovesResponseToJson(MovesResponse instance) =>
    <String, dynamic>{
      'move': instance.move,
    };

MovesData _$MovesDataFromJson(Map<String, dynamic> json) => MovesData(
      json['name'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$MovesDataToJson(MovesData instance) => <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };

TypesResponse _$TypesResponseFromJson(Map<String, dynamic> json) =>
    TypesResponse(
      json['type'] == null
          ? null
          : TypesData.fromJson(json['type'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TypesResponseToJson(TypesResponse instance) =>
    <String, dynamic>{
      'type': instance.type,
    };

TypesData _$TypesDataFromJson(Map<String, dynamic> json) => TypesData(
      json['name'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$TypesDataToJson(TypesData instance) => <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };

GameIndicesResponse _$GameIndicesResponseFromJson(Map<String, dynamic> json) =>
    GameIndicesResponse(
      json['version'] == null
          ? null
          : GameIndicesData.fromJson(json['version'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GameIndicesResponseToJson(
        GameIndicesResponse instance) =>
    <String, dynamic>{
      'version': instance.version,
    };

GameIndicesData _$GameIndicesDataFromJson(Map<String, dynamic> json) =>
    GameIndicesData(
      json['name'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$GameIndicesDataToJson(GameIndicesData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };
