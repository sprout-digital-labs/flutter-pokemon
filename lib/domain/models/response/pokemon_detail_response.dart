import 'package:json_annotation/json_annotation.dart';

part 'pokemon_detail_response.g.dart';

@JsonSerializable()
class PokemonDetailResponse {
  PokemonDetailResponse(this.gameIndices, this.moves, this.types, this.species, this.stats, this.weight, this.height,
      {this.abilities, this.sprites});

  final List<AbilitiesResponse>? abilities;
  final SpritesResponse? sprites;
  @JsonKey(name: "game_indices")
  final List<GameIndicesResponse>? gameIndices;
  final List<MovesResponse>? moves;
  final List<TypesResponse>? types;
  final List<BaseStatsResponse>? stats;
  final SpeciesResponse? species;
  final int? weight;
  final int? height;

  factory PokemonDetailResponse.fromJson(Map<String, dynamic> json) {
    return _$PokemonDetailResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PokemonDetailResponseToJson(this);
}

@JsonSerializable()
class BaseStatsResponse {
  BaseStatsResponse(this.baseStat, this.stat);

  @JsonKey(name: "base_stat")
  final int? baseStat;
  final BaseStatsData? stat;

  factory BaseStatsResponse.fromJson(Map<String, dynamic> json) {
    return _$BaseStatsResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BaseStatsResponseToJson(this);
}

@JsonSerializable()
class BaseStatsData {
  BaseStatsData(this.name);

  final String? name;

  factory BaseStatsData.fromJson(Map<String, dynamic> json) {
    return _$BaseStatsDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BaseStatsDataToJson(this);
}

@JsonSerializable()
class SpeciesResponse {
  SpeciesResponse(this.name);

  final String? name;

  factory SpeciesResponse.fromJson(Map<String, dynamic> json) {
    return _$SpeciesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SpeciesResponseToJson(this);
}

@JsonSerializable()
class AbilitiesResponse {
  AbilitiesResponse(this.slot, this.ability, this.isHidden);

  final int? slot;
  @JsonKey(name: "is_hidden")
  final bool? isHidden;
  final AbilityData? ability;

  factory AbilitiesResponse.fromJson(Map<String, dynamic> json) {
    return _$AbilitiesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AbilitiesResponseToJson(this);
}

@JsonSerializable()
class AbilityData {
  AbilityData(this.name, this.url);

  final String? name;
  final String? url;

  factory AbilityData.fromJson(Map<String, dynamic> json) {
    return _$AbilityDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AbilityDataToJson(this);
}

@JsonSerializable()
class SpritesResponse {
  SpritesResponse(this.frontShinyFemale, this.backDefault, this.backFemale, this.backShiny, this.backShinyFemale,
      this.frontDefault, this.frontFemale, this.frontShiny);

  @JsonKey(name: "back_default")
  final String? backDefault;
  @JsonKey(name: "back_female")
  final String? backFemale;
  @JsonKey(name: "back_shiny")
  final String? backShiny;
  @JsonKey(name: "back_shiny_female")
  final String? backShinyFemale;
  @JsonKey(name: "front_default")
  final String? frontDefault;
  @JsonKey(name: "front_female")
  final String? frontFemale;
  @JsonKey(name: "front_shiny")
  final String? frontShiny;
  @JsonKey(name: "front_shiny_female")
  final String? frontShinyFemale;

  factory SpritesResponse.fromJson(Map<String, dynamic> json) {
    return _$SpritesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SpritesResponseToJson(this);
}

@JsonSerializable()
class MovesResponse {
  MovesResponse(this.move);

  final MovesData? move;

  factory MovesResponse.fromJson(Map<String, dynamic> json) {
    return _$MovesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$MovesResponseToJson(this);
}

@JsonSerializable()
class MovesData {
  MovesData(this.name, this.url);

  final String? name;
  final String? url;

  factory MovesData.fromJson(Map<String, dynamic> json) {
    return _$MovesDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$MovesDataToJson(this);
}

@JsonSerializable()
class TypesResponse {
  TypesResponse(this.type);

  final TypesData? type;

  factory TypesResponse.fromJson(Map<String, dynamic> json) {
    return _$TypesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TypesResponseToJson(this);
}

@JsonSerializable()
class TypesData {
  TypesData(this.name, this.url);

  final String? name;
  final String? url;

  factory TypesData.fromJson(Map<String, dynamic> json) {
    return _$TypesDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TypesDataToJson(this);
}

@JsonSerializable()
class GameIndicesResponse {
  GameIndicesResponse(this.version);

  final GameIndicesData? version;

  factory GameIndicesResponse.fromJson(Map<String, dynamic> json) {
    return _$GameIndicesResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GameIndicesResponseToJson(this);
}

@JsonSerializable()
class GameIndicesData {
  GameIndicesData(this.name, this.url);

  final String? name;
  final String? url;

  factory GameIndicesData.fromJson(Map<String, dynamic> json) {
    return _$GameIndicesDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$GameIndicesDataToJson(this);
}
