// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_detail_args.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameDetailArgs _$GameDetailArgsFromJson(Map<String, dynamic> json) =>
    GameDetailArgs(
      json['title'] as String,
      json['id'] as int,
    );

Map<String, dynamic> _$GameDetailArgsToJson(GameDetailArgs instance) =>
    <String, dynamic>{
      'title': instance.title,
      'id': instance.id,
    };
