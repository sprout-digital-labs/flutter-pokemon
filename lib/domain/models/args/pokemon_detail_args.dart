import 'package:flutter/material.dart';

class PokemonDetailArgs {
  String title;
  int id;
  Color bagColor;

  PokemonDetailArgs(this.title, this.id, this.bagColor);
}
