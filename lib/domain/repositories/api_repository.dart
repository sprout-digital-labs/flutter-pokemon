

import '../models/response/pokemon_detail_response.dart';
import '../models/response/pokemon_list_response.dart';

abstract class ApiRepository {
  Future<PokemonListResponse> fetchPokemonList(int page, {String? url});

  Future<PokemonDetailResponse> fetchPokemonDetail(int gameId);
}
