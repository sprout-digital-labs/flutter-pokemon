import 'package:injectable/injectable.dart';

import '../../domain/models/response/pokemon_detail_response.dart';
import '../../domain/models/response/pokemon_list_response.dart';
import '../../domain/repositories/api_repository.dart';
import 'base/api_service.dart';

@Injectable(as: ApiRepository)
class ApiRepositoryImpl implements ApiRepository {
  ApiRepositoryImpl({required this.baseService});

  final ApiService baseService;

  @override
  Future<PokemonListResponse> fetchPokemonList(int page, {String? url}) async {
    var res = await baseService.fetchGameList(page: page, url: url);
    return PokemonListResponse.fromJson(res);
  }

  @override
  Future<PokemonDetailResponse> fetchPokemonDetail(int pokemonId) async {
    var res = await baseService.fetchGameDetail(pokemonId);
    return PokemonDetailResponse.fromJson(res);
  }
}
