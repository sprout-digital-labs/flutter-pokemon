import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import '../../../utils/resources/env.dart';
import '../../../config/network/app_interceptor.dart';

abstract class ApiService {
  Future<dynamic> fetchGameList({String? url, int? page = 1});

  Future<dynamic> fetchGameDetail(int code);
}

@Injectable(as: ApiService)
class ApiServiceImpl implements ApiService {
  ApiServiceImpl({required this.dio});

  final Dio dio;

  Future<Response<dynamic>?> _get(String url, {Map<String, dynamic>? queryParameters}) async {
    return await dio.get(url, queryParameters: queryParameters);
  }

  @override
  Future<dynamic> fetchGameList({String? url, int? page = 0}) async {
    try {
      Response<dynamic>? response = await _get(url ?? "", queryParameters: {
        "limit": 10,
        "offset": (page ?? 0) * 10,
      });

      return response?.data;
    } on DioException {
      throw DioException;
    }
  }

  @override
  Future<dynamic> fetchGameDetail(int code) async {
    try {
      Response<dynamic>? response =
          await _get("/$code",);

      return response?.data;
    } on DioException {
      throw DioException;
    }
  }
}

Dio buildDio() {
  var dio = Dio();

  dio.interceptors.add(DioInterceptors());
  dio.interceptors.add(NetworkInterceptors(dio));

  dio.options.baseUrl = Env.data.apiUrl;
  dio.options.contentType = Headers.jsonContentType;
  dio.options.connectTimeout = const Duration(minutes: 10);
  dio.options.receiveTimeout = const Duration(minutes: 10);
  dio.options.sendTimeout = const Duration(minutes: 10);

  return dio;
}
